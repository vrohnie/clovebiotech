package au.edu.wehi.clove;


import java.util.*;

public class ComplexEvent extends Event{

    private Event[] eventsInvolvedInComplexEvent;
    private GenomicCoordinate[] insertionPoint;


    public ComplexEvent(GenomicCoordinate c1, GenomicCoordinate c2,
                        EVENT_TYPE type, Event[] involvedEvents, Boolean addition, GenomicNode hostingNode) {
        super(c1, c2, type);
        this.eventsInvolvedInComplexEvent = involvedEvents;

        for(Event e: involvedEvents){
            super.addCaller(e.getCalledBy());
            super.increaseCalls(e.getCalledTimes());
        }

        this.setAlt(getAltVCF(type));

        String id = "";
        Double qual = 0.0;
        Set<String> filterSet = new LinkedHashSet<>();
        Set<GenomicNode> nodes = new LinkedHashSet<GenomicNode>();

        for(Event e: involvedEvents){
            id = id + e.getId() + (addition?"+":"-");
            qual += e.getQual();

            String[] curfilters = e.getFilter().split(";");
            filterSet.addAll(Arrays.asList(curfilters));
            nodes.add(e.getNode(true));
            nodes.add(e.getNode(false));
        }

        id = id.substring(0, id.length() - 1);
        qual = qual/involvedEvents.length;

        if(filterSet.size() > 1){
            filterSet.remove("PASS");
        }

        this.setFilter(String.join(";", filterSet));
        this.setId(id);
        this.setRef(involvedEvents[0].getRef());
        this.setQual(qual);
        ArrayList<GenomicNode> nodesList = new ArrayList();

        while (nodes.remove(null));
        nodesList.addAll(nodes);

//        if(type == EVENT_TYPE.COMPLEX_INVERTED_REPLACED_DELETION){
//            System.out.println(nodesList);
//        }
        this.setNodes(nodesList);

        this.getInfo().put("SVTYPE",this.getSvTypeVCF(this.getType()));
        this.getInfo().put("CHR2",this.getC2().getChr());
        this.getInfo().put("END",String.valueOf(this.getC2().getPos()));

//		tempInfo="SVTYPE="+newComplexEvent.getAlt().substring(1, 4)+";CHR2="+eventStart.getChr()+";START="+Integer.toString(eventStart.getPos())+";END="+Integer.toString(eventEnd.getPos())+";ADP="+readDepth;
//		newComplexEvent.setInfo(tempInfo);
//		newComplexEvent.setCoord(eventInsert);

    }

    public ComplexEvent(GenomicCoordinate c1, GenomicCoordinate c2,
                        EVENT_TYPE type, Event[] involvedEvents, Boolean addition , GenomicNode hostingNode, GenomicCoordinate[] insertionPoint) {
        this(c1,c2,type,involvedEvents, addition, hostingNode);
        this.insertionPoint = insertionPoint;

        this.getInfo().put("CHR_C",insertionPoint[0].getChr());

        if(insertionPoint[1] != null){
            int start = Math.min(insertionPoint[0].getPos(), insertionPoint[1].getPos());
            int end = Math.max(insertionPoint[0].getPos(), insertionPoint[1].getPos());
            this.getInfo().put("START_C",String.valueOf(start));
            this.getInfo().put("END_C",String.valueOf(end));
        } else {
            this.getInfo().put("START_C",String.valueOf(insertionPoint[0].getPos()));
        }

    }


    public Event[] getEventsInvolvedInComplexEvent(){
        return this.eventsInvolvedInComplexEvent;
    }

    public GenomicCoordinate[] getInsertionPoint() {
        return this.insertionPoint;
    }

    @Override
    public String toString(){
        if(this.getType() == EVENT_TYPE.COMPLEX_TRANSLOCATION || this.getType() == EVENT_TYPE.COMPLEX_DUPLICATION
                || this.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_DUPLICATION || this.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION
                || this.getType() == EVENT_TYPE.COMPLEX_INVERTED_DUPLICATION || this.getType() == EVENT_TYPE.COMPLEX_INVERTED_TRANSLOCATION
                || this.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_DUPLICATION || this.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION){
            return this.getInsertionPoint()+" "+super.toString();
        } else {
            return super.toString();
        }
    }

}
