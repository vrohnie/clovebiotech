package au.edu.wehi.clove;

import htsjdk.samtools.SamReader;
import htsjdk.samtools.util.Interval;
import htsjdk.samtools.util.IntervalList;
import htsjdk.samtools.util.SamLocusIterator;
import htsjdk.samtools.util.Tuple;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


enum EVENT_TYPE {
    // simple event types
    INS, INV1, INV2, DEL, TAN, INVTX1, INVTX2, ITX1, ITX2, BE1, BE2, XXX,
    // inversion
    COMPLEX_INVERSION,
    // duplication events
    COMPLEX_INVERTED_DUPLICATION, COMPLEX_DUPLICATION,
    COMPLEX_INTERCHROMOSOMAL_DUPLICATION, COMPLEX_INTERCHROMOSOMAL_INVERTED_DUPLICATION,
    // translocation events
    COMPLEX_TRANSLOCATION, COMPLEX_INVERTED_TRANSLOCATION, COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION,
    COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION,
    // insertion events
    COMPLEX_BIG_INSERTION,
    //knockout events
    COMPLEX_REPLACED_DELETION, COMPLEX_INVERTED_REPLACED_DELETION,
    // balanced breakpoints
    COMPLEX_BP, COMPLEX_INVERTED_BP,
    // vector integrations
    VECTOR_PARTS}

public class Event {

    private GenomicCoordinate c1, c2;
    private EVENT_TYPE type;
    private ArrayList<GenomicNode> myNodes;
    private String id;
    private String ref;
    private String alt;
    private Double qual;
    private String filter;
    private HashMap<String,String> info;
    private HashSet<Clove.SV_ALGORITHM> calledBy;
    private int calledTimes;
    private boolean used;

    public Event(GenomicCoordinate c1, GenomicCoordinate c2, EVENT_TYPE type){

        if(c1.compareTo(c2) <= 0){
            this.c1 = c1;
            this.c2 = c2;
        } else {
            this.c1 = c2;
            this.c2 = c1;
        }

        this.type = type;
        myNodes = new ArrayList<GenomicNode>();
        this.info=new HashMap();

        this.info.put("SVTYPE",type.toString());
        this.info.put("CHR2",this.c2.getChr());
        this.info.put("END",String.valueOf(this.c2.getPos()));

        this.calledBy = new HashSet<Clove.SV_ALGORITHM>();
        this.calledTimes = 0;
        this.used = false;
    }

    public Event(GenomicCoordinate c1, GenomicCoordinate c2, EVENT_TYPE type, String additionalInformation){
        this(c1,c2,type);
        this.info.put("INSERTION",additionalInformation);
    }

    /*Create event with VCF Info*/
    public Event(GenomicCoordinate c1, GenomicCoordinate c2, EVENT_TYPE type, String id, String ref, String alt, String qual, String filter, HashMap<String, String> info, HashSet<Clove.SV_ALGORITHM> calledBy, int calledTimes){

        if(c1.compareTo(c2) < 0){
            this.c1 = c1;
            this.c2 = c2;
        } else {
            this.c1 = c2;
            this.c2 = c1;
        }

        this.type = type;
        myNodes = new ArrayList<GenomicNode>();
        this.id=id;
        this.ref=ref;
        this.alt=alt;

        try{
            this.qual = Double.parseDouble(qual);
        } catch (NumberFormatException ex){
            this.qual = 0.0;
        }

        this.filter=filter;
        if(info != null){
            this.info=info;
        } else {
            this.info = new HashMap<>();
        }

        this.info.put("SVTYPE",type.toString());
        this.info.put("CHR2",this.c2.getChr());
        this.info.put("END",String.valueOf(this.c2.getPos()));

        this.calledBy = calledBy;
        this.calledTimes = calledTimes;
    }

    /*
     * Static function to handle the particularities of Socrates output, and convert it into a general
     * purpose Event.
     */
    public static Event createNewEventFromSocratesOutputLatest(String output, int count){
        String line = output.replace("\t\t", "\tX\t");
        StringTokenizer t = new StringTokenizer(line);
        String chr1 = t.nextToken(":");
        int p1 = Integer.parseInt(t.nextToken(":\t"));
        String o1 = t.nextToken("\t");
        t.nextToken("\t");
        String chr2 = t.nextToken("\t:");
        int p2 = Integer.parseInt(t.nextToken("\t:"));
        String o2 = t.nextToken("\t");

        String id="SOC"+Integer.toString(count);
        String ref=".";
        String qual=".";
        String filter="PASS";

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
        EVENT_TYPE type = classifySocratesBreakpoint(c1, o1, c2, o2);

        //look for additional information at the end of the call
        int i = 0;
        while(i<19 && t.hasMoreTokens()){
            i++;
            t.nextToken();
        }
        String additionalComments = (t.hasMoreTokens()? t.nextToken() : "");
        if(additionalComments.startsWith("Inserted sequence")){
            String insert = additionalComments.substring("Inserted sequence: ".length());
            return new Event(c1, c2, type, insert);
        }

        String alt= getAltVCF(type);
        // String info="SVTYPE="+alt.substring(1, 4)+";CHR2="+chr2+";END="+p2;

        return new Event(c1, c2, type, id, ref, alt, qual, filter, null, new HashSet<Clove.SV_ALGORITHM>() {{add(Clove.SV_ALGORITHM.SOCRATES);}}, 1);
    }
    /*
     * Function to classify a line of Socrates output into a genomic event type.
     * The distinctions between INV1/2 etc are arbitrary, and have to be consistent across all the inputs.
     */
    private static EVENT_TYPE classifySocratesBreakpoint(GenomicCoordinate c1, String o1, GenomicCoordinate c2, String o2) {

        if(c1.compareTo(c2)>0){
            GenomicCoordinate tmp;
            String tmp_o;
            tmp = new GenomicCoordinate(c1.getChr(), c1.getPos());
            c1.setChr(c2.getChr());
            c1.setPos(c2.getPos());
            c2.setChr(tmp.getChr());
            c2.setPos(tmp.getPos());
            tmp_o = o1;
            o1 = o2;
            o2 = tmp_o;
        }

        if (o2.equals("")) {
            if (o1.equals("+")) {
                return EVENT_TYPE.BE1;
            } else {
                return EVENT_TYPE.BE2;
            }
        } else if (o1.equals(o2)) {
            if (c1.onSameChromosome(c2)) {
                if (o1.equals("+"))
                    return EVENT_TYPE.INV1;
                else
                    return EVENT_TYPE.INV2;
            } else {
                if (o1.equals("+"))
                    return EVENT_TYPE.INVTX1;
                else
                    return EVENT_TYPE.INVTX2;
            }
        } else {
            if (c1.onSameChromosome(c2)) {
                if (o1.equals("+") && c1.compareTo(c2) < 0 || o1.equals("-") && c1.compareTo(c2) >= 0) {
                    return EVENT_TYPE.DEL;
                } else {
                    return EVENT_TYPE.TAN;
                }
            } else {
                if (o1.equals("+") && c1.compareTo(c2) < 0 || o1.equals("-") && c1.compareTo(c2) >= 0) {
                    return EVENT_TYPE.ITX1;
                } else {
                    return EVENT_TYPE.ITX2;
                }
            }
        }
    }

    /*
     * Static function to handle the particularities of Delly output, and convert it into a general
     * purpose Event.
     */
    public static Event createNewEventFromDellyOutput(String output){
        StringTokenizer t = new StringTokenizer(output, "\t:");
        String chr1 = t.nextToken();
        String chr2 = chr1;
        int p1 = Integer.parseInt(t.nextToken());
        int p2 = Integer.parseInt(t.nextToken());
        t.nextToken();
        t.nextToken();
        t.nextToken();
        String typeT;
        String tempT = t.nextToken();
        typeT = tempT.substring(1,tempT.indexOf("_"));
        if (typeT.equals("Inversion")){
            typeT = tempT.substring(1,(tempT.indexOf("_")+2));
        }

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
        EVENT_TYPE type = classifyDellyBreakpoint(c1, c2, typeT);

        //System.out.println(chr1 +"\t"+ p1 +"\t"+ p2 +"\t" + type +"\t"+ typeT);

        return new Event(c1, c2, type);
    }

    /*
     * Function to classify a line of Delly output into a genomic event type.
     * The distinctions between INV1/2 etc are arbitrary, and have to be consistent across all the inputs.
     * c1 and c2 are always the same chromosome
     */
    private static EVENT_TYPE classifyDellyBreakpoint(GenomicCoordinate c1, GenomicCoordinate c2, String t){
        if(t.equals("Inversion_0")){
            return EVENT_TYPE.INV1;
        } else if (t.equals("Inversion_1")){
            return EVENT_TYPE.INV2;
        } else if (t.equals("Deletion")){
            return EVENT_TYPE.DEL;
        } else if (t.equals("Duplication")){
            return EVENT_TYPE.TAN;
        } else {
            return EVENT_TYPE.XXX;
        }
    }


    /*
     * Static function to handle the particularities of Delly output, and convert it into a general
     * purpose Event.
     */
    public static Event createNewEventFromDellyOutputLatest(String output){
        String[] bits = output.split("\t");
        String chr1 = bits[0];
        int p1 = Integer.parseInt(bits[1]);
        String[] moreBits = bits[7].split(";");
        String chr2 = moreBits[5].replace("CHR2=", "");
        int p2 = Integer.parseInt(moreBits[6].replace("END=", ""));
        String o = moreBits[7].replace("CT=", "");
        String o1 = (Integer.parseInt(o.split("to")[0]) == 3? "+" : "-");
        String o2 = (Integer.parseInt(o.split("to")[1]) == 3? "+" : "-");

        String id=bits[2];
        String ref=bits[3];
        String alt=bits[4];
        String qual=bits[5];
        String filter=bits[6];
        // String info=bits[7];

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
        EVENT_TYPE type = classifySocratesBreakpoint(c1, o1, c2, o2);

        //System.out.println(chr1 +"\t"+ p1 +"\t"+ p2 +"\t" + type +"\t"+ typeT);
        // info="SVTYPE="+type+";CHR2="+chr2+";END="+p2;

        return new Event(c1, c2, type, id, ref, alt, qual, filter, null, new HashSet<Clove.SV_ALGORITHM>() {{add(Clove.SV_ALGORITHM.DELLY);}}, 1);
        //return new Event(c1, c2, type);

    }

    public static Event createNewEventFromDelly2Output(String output){
        String[] bits = output.split("\t");
        String chr1 = bits[0];
        int p1 = Integer.parseInt(bits[1]);

        HashMap<String,String> info = vcfInfoToHashMap(bits[7]);

        // String[] moreBits = bits[7].split(";");
        // String chr2 = moreBits[3].replace("CHR2=", "");
//        int p2 = Integer.parseInt(moreBits[4].replace("END=", ""));
//        String o = moreBits[7].replace("CT=", "");
//        String o1 = (Integer.parseInt(o.split("to")[0]) == 3? "+" : "-");
//        String o2 = (Integer.parseInt(o.split("to")[1]) == 3? "+" : "-");

        String chr2 = info.get("CHR2");
        int p2 = -1;

        if(chr2 == null){
            chr2 = chr1;
            p2 = Integer.parseInt(info.get("END"));
        } else {
            p2 = Integer.parseInt(info.get("POS2"));
        }


        String o = info.get("CT");
        String o1 = (Integer.parseInt(o.split("to")[0]) == 3? "+" : "-");
        String o2 = (Integer.parseInt(o.split("to")[1]) == 3? "+" : "-");

        String id=bits[2];
        String ref=bits[3];
        String alt=bits[4];
        String qual=bits[5];
        String filter=bits[6];
        // String info=bits[7];

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
        EVENT_TYPE type = classifySocratesBreakpoint(c1, o1, c2, o2);

        //System.out.println(chr1 +"\t"+ p1 +"\t"+ p2 +"\t" + type +"\t"+ typeT);

        return new Event(c1, c2, type, id, ref, alt, qual, filter, null, new HashSet<Clove.SV_ALGORITHM>() {{add(Clove.SV_ALGORITHM.DELLY2);}}, 1);
        //return new Event(c1, c2, type);

    }
    /*
     * Function to classify a line of BedPE into a genomic event type.
     * The distinctions between INV1/2 etc are arbitrary, and have to be consistent across all the inputs.
     * c1 and c2 are always the same chromosome
     */
    private static EVENT_TYPE classifyDellyBreakpointLatest(GenomicCoordinate c1, GenomicCoordinate c2){
        return null;
    }

    /*
     * Static function to handle the particularities of MetaSV output, and convert it into a general
     * purpose Event.
     */
    public static Event createNewEventFromMetaSVOutput(String output){
        String[] bits = output.split("\t");
        String chr1 = bits[0];
        int p1 = Integer.parseInt(bits[1]);
        String[] moreBits = bits[7].split(";");
        String chr2 = chr1;
        int p2 = 0;
        String o1 = null;
        String o2 = null;
        for(String s: moreBits){
            if(s.startsWith("CHR2"))
                chr2 = s.replace("CHR2=", "");
            else if(s.startsWith("END="))
                p2 = Integer.parseInt(s.replace("END=", ""));
            else if(s.startsWith("SVLEN="))
                p2 = p1 + Integer.parseInt(s.replace("SVLEN=", ""));
            else if(s.startsWith("BD_ORI1")){
                String o = s.replace("BD_ORI1=", "");
                int fwd = Integer.parseInt(o.split("[+-]")[0]);
                int rev = Integer.parseInt(o.split("[+-]")[1]);
                o1 = (fwd>rev ? "+" : "-");}

            else if(s.startsWith("BD_ORI2")){
                String o = s.replace("BD_ORI2=", "");
                int fwd = Integer.parseInt(o.split("[+-]")[0]);
                int rev = Integer.parseInt(o.split("[+-]")[1]);
                o2 = (fwd>rev ? "+" : "-");
            }
        }
        String id=bits[2];
        String ref=bits[3];
        String alt=bits[4];
        String qual=bits[5];
        String filter=bits[6];
        String info=bits[7];

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
        EVENT_TYPE type = classifyMetaSVBreakpoint(alt, chr1, chr2, o1, o2);

        if(type == EVENT_TYPE.COMPLEX_INVERSION){
            Event e =  new ComplexEvent(c1, c2, type, new Event[] {}, true,null);
            e.setAlt("<CIV>");
            return e;
        }

        //System.out.println(chr1 +"\t"+ p1 +"\t"+ p2 +"\t" + type +"\t"+ typeT);

        return new Event(c1, c2, type, id, ref, alt, qual, filter, vcfInfoToHashMap(info), new HashSet<Clove.SV_ALGORITHM>() {{add(Clove.SV_ALGORITHM.METASV);}}, 1);
        //return new Event(c1, c2, type);

    }

    private static EVENT_TYPE classifyMetaSVBreakpoint(String t, String c1, String c2, String o1, String o2){
        if(t.equals("<DEL>")){
            return EVENT_TYPE.DEL;
        } else if (t.equals("<INS>")){
            return EVENT_TYPE.INS;
        } else if (t.equals("<INV>")){
            return EVENT_TYPE.COMPLEX_INVERSION;
        } else if(t.equals("<ITX>")){
            if(o1.equals("+"))
                if(o2.equals("+"))
                    return EVENT_TYPE.INV1;
                else
                    return EVENT_TYPE.DEL;
            else if(o2.equals("+"))
                return EVENT_TYPE.TAN;
            else
                return EVENT_TYPE.INV2;
        } else if (t.equals("<CTX>")) {
            if(o1.equals(o2)) {
                if(o1.equals("+"))
                    return EVENT_TYPE.INVTX1;
                else
                    return EVENT_TYPE.INVTX2;
            } else if(o1.equals("+") &&  c1.compareTo(c2) < 0 || o1.equals("-") && c1.compareTo(c2) >= 0){
                return EVENT_TYPE.ITX1;
            } else {
                return EVENT_TYPE.ITX2;
            }
        } else if(t.equals("<DUP>")){
            return EVENT_TYPE.TAN;
        }
        else {
            return EVENT_TYPE.XXX;
        }
    }

    public static Event createNewEventFromBEDPE (String output){
        String[] bits = output.split("\t");
        String chr1 = bits[0];
        int p1 = Integer.parseInt(bits[1]);
        String chr2 = bits[3];
        int p2 = Integer.parseInt(bits[4]);
        String o1 = bits[8];
        String o2 = bits[9];
        String qual=bits[7];
        String id=bits[6];
        String ref="";
        String alt="";
        String info= (bits.length>10? bits[10]+";": "");
        String filter = "PASS";
        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
        EVENT_TYPE type = classifySocratesBreakpoint(c1, o1, c2, o2);
        // info+="SVTYPE="+type+";CHR2="+chr2+";END="+p2;
        return new Event(c1, c2, type, id, ref, alt, qual, filter, null, new HashSet<Clove.SV_ALGORITHM>() {{add(Clove.SV_ALGORITHM.BEDPE);}}, 1);
    }


    public static Event createNewEventFromCrestOutput(String output) {
        StringTokenizer t = new StringTokenizer(output, "\t");

        String chr1 = t.nextToken();
        int p1 = Integer.parseInt(t.nextToken());
        String o1 = t.nextToken();
        t.nextToken();
        String chr2 = t.nextToken();
        int p2 = Integer.parseInt(t.nextToken());
        String o2 = t.nextToken();

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);

        t.nextToken();
        EVENT_TYPE type = classifyCrestBreakpoint(t.nextToken(), chr1, chr2, o1, o2);

        if(type == EVENT_TYPE.COMPLEX_INVERSION){
            Event e =  new ComplexEvent(c1, c2, type, new Event[] {}, true, null);
            e.setAlt("<CIV>");
            return e;
        }
        return new Event(c1, c2, type);
    }

    public static Event createNewEventFromCrestOutputLatest(String output, int count) {
        StringTokenizer t = new StringTokenizer(output, "\t");

        String chr1 = t.nextToken();
        int p1 = Integer.parseInt(t.nextToken());
        String o1 = t.nextToken();
        t.nextToken();
        String chr2 = t.nextToken();
        int p2 = Integer.parseInt(t.nextToken());
        String o2 = t.nextToken();

        String id="CRT"+Integer.toString(count);
        String ref=".";
        String qual=".";
        String filter="PASS";

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);

        t.nextToken();
        EVENT_TYPE type = classifyCrestBreakpoint(t.nextToken(), chr1, chr2, o1, o2);

        if(type == EVENT_TYPE.COMPLEX_INVERSION){
            Event e =  new ComplexEvent(c1, c2, type, new Event[] {}, true, null);
            e.setAlt("<CIV>");
            return e;
        }

        String alt= getAltVCF(type);
        // String info="SVTYPE="+alt.substring(1, 4)+";CHR2="+chr2+";END="+p2;

        return new Event(c1, c2, type, id, ref, alt, qual, filter, null, new HashSet<Clove.SV_ALGORITHM>() {{add(Clove.SV_ALGORITHM.CREST);}}, 1);
    }

    private static EVENT_TYPE classifyCrestBreakpoint(String t, String c1, String c2, String o1, String o2){
        if(t.equals("DEL")){
            return EVENT_TYPE.DEL;
        } else if (t.equals("INS")){
            return EVENT_TYPE.TAN;
        } else if (t.equals("INV")){
            return EVENT_TYPE.COMPLEX_INVERSION;
        } else if(t.equals("ITX")){
            if(o1.equals("+"))
                return EVENT_TYPE.INV1;
            else
                return EVENT_TYPE.INV2;
        } else if (t.equals("CTX")) {
//			if(o1.equals(o2)) {
//				if(o1.equals("+"))
//					return EVENT_TYPE.INVTX1;
//				else
//					return EVENT_TYPE.INVTX2;
//			} else if(o1.equals("+") &&  c1.compareTo(c2) < 0 || o1.equals("-") && c1.compareTo(c2) >= 0){
//				return EVENT_TYPE.ITX1;
//			} else {
//				return EVENT_TYPE.ITX2;
//			}
            if(o1.equals(o2)) {
                if(o1.equals("+")) {
                    if(c1.compareTo(c2) < 0)
                        return EVENT_TYPE.ITX1;
                    else
                        return EVENT_TYPE.ITX2;
                }
                else
                    return EVENT_TYPE.XXX;
//			} else if(o1.equals("+") &&  c1.compareTo(c2) < 0 || o1.equals("-") && c1.compareTo(c2) >= 0){
            } else if(c1.compareTo(c2) < 0 && o1.equals("-")){
                return EVENT_TYPE.INVTX2;
            } else if(c1.compareTo(c2) < 0 && o1.equals("+")){
                return EVENT_TYPE.INVTX1;
            }
            else if (c1.compareTo(c2) >= 0 && o1.equals("+")){
                return EVENT_TYPE.INVTX1;
            } else {
                return EVENT_TYPE.INVTX2;
            }
        } else {
            return EVENT_TYPE.XXX;
        }
    }


    public static Event createNewEventFromGustafOutput(String output) {
        StringTokenizer t = new StringTokenizer(output, "\t");

        String chr1 = t.nextToken();
        t.nextToken();
        EVENT_TYPE type = classifyGustafBreakpoint(t.nextToken());
        int p1 = Integer.parseInt(t.nextToken());
        int p2 = Integer.parseInt(t.nextToken());
        t.nextToken();
        String o = t.nextToken();
        if(type==EVENT_TYPE.INV1 && o.equals("-"))
            type = EVENT_TYPE.INV2;
        t.nextToken();
        String info = t.nextToken();
        StringTokenizer i = new StringTokenizer(info, "=;");
        i.nextToken();
        i.nextToken();
        String d = i.nextToken();
        String chr2;
        if(d.equals("endChr")){
            chr2 = i.nextToken();
            i.nextToken();
            p2 = Integer.parseInt(i.nextToken());
        } else if (d.equals("size")) {
            chr2 = chr1;
        } else {
            System.err.println("Confusion in the Gustaf camp!");
            chr2=null;
        }

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);

        return new Event(c1, c2, type);
    }
    private static EVENT_TYPE classifyGustafBreakpoint(String t){
        if(t.equals("deletion")){
            return EVENT_TYPE.DEL;
        } else if (t.equals("duplication")){
            return EVENT_TYPE.TAN;
        } else if (t.equals("inversion")){
            return EVENT_TYPE.INV1;
        } else if(t.equals("ITX")){
            return EVENT_TYPE.INV1;
        } else if (t.equals("insertion")) {
            return EVENT_TYPE.INS;
        } else {
            return EVENT_TYPE.XXX;
        }
    }



    /*************************************/
    /* GRIDSS Output                    */
    /*****************************************************************************************************************/

    public static Event createNewEventFromGRIDSSOutput(String output) {

        //System.out.println(output);

        Pattern pattern;
        Matcher matcher;

        String[] bits = output.split("\t");

//        System.out.println(output + "\n" + bits.length);
//        if(bits.length != 7){
//        	System.err.println("Corrupted VCF File");
//        	System.exit(1 );
//        	return null;
//		}

        String chr1 = bits[0], chr2 = "";
        String orientation1 = "", orientation2 = "";
        int p1 = Integer.parseInt(bits[1]), p2 = -1;
        String alt = bits[4];
        String[] result = Event.classifyAltGridssLumpy(alt);

        chr2 = result[0];
        orientation1 = result[2];
        orientation2 = result[3];

        if(!result[1].isEmpty()) {
            p2 = Integer.parseInt(result[1]);
        }

        HashMap<String,String> info = new HashMap<>();

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
        EVENT_TYPE type = EVENT_TYPE.XXX;

        if (!orientation1.equals("")) {
//        	 System.out.println(c1.toString() + " " + orientation1 + " " + c2.toString());
            type = Event.classifySocratesBreakpoint(c1, orientation1, c2, orientation2);
//			 System.out.println(c1.toString() + " " + orientation1 + " " +  " " + c2.toString() + type);

            // info="SVTYPE="+type+";CHR2="+chr2+";END="+p2;

            if(!result[4].equals("")){
                info.put("INSERTION",result[4]);
            }
        }

        pattern = Pattern.compile("SVTYPE=(.+?)");
        matcher = pattern.matcher(bits[7]);
        matcher.find();

        String id = bits[2];
        String ref = bits[3];
        String qual = bits[5];
        String filter = bits[6];
//        if (matcher.groupCount() != 0){
//        	System.out.println(matcher.toString());
//            info = matcher.group(1);
//        }

        return new Event(
                c1, c2, type, id, ref, alt, qual, filter, info,
                new HashSet<Clove.SV_ALGORITHM>() {{add(Clove.SV_ALGORITHM.GRIDSS);}}, 1
        );
    }


    private static String[] classifyAltGridssLumpy(String alt) {

        /* 0: chr2
         * 1: p2
         * 2: orientation 1
         * 3: orientation 2
         * */
        String[] result = new String[5];
        result[4] = "";

        if(alt.contains("]")) {

            String[] item = alt.split("]");
            if (item.length == 3){
                // ALT: ]p]t --> len 3
                result[0] = item[1].split(":")[0];
                result[1] = item[1].split(":")[1];

                result[2] = "-";
                result[3] = "+";
                result[4] = item[2];

            } else {
                // ALT: t]p] --> len 2
                result[0] = item[1].split(":")[0];
                result[1] = item[1].split(":")[1];

                result[2] = "+";
                result[3] = "+";
                result[4] = item[0];
            }

        } else if(alt.contains("[")){

            String[] item = alt.split("\\[");
            if (item.length == 3) {
                // ALT: [p[t --> become 3
                result[0] = item[1].split(":")[0];
                result[1] = item[1].split(":")[1];

                result[2] = "-";
                result[3] = "-";
                result[4] = item[2];

            } else {
                // ALT: t[p[ --> become 2
                result[0] = item[1].split(":")[0];
                result[1] = item[1].split(":")[1];

                result[2] = "+";
                result[3] = "-";
                result[4] = item[0];

            }
        } else if(alt.contains(".")) {
            result[0] = "";
            result[1] = "";

            result[4] = alt;

            if(alt.startsWith(".")) {
                result[2] = "-";
            } else {
                result[2] = "+";
            }

            result[3] = "";

        } else {
            // leave empty
            System.err.println("Found unknown ALT in GRIDSS input.");
        }

        return result;
    }

    /*************************************/
    /* LUMPY Output                      */
    /*****************************************************************************************************************/
    public static Event createNewEventFromLUMPYOutput(String output) {

        Pattern pattern; Matcher matcher;
        String orientation1 = "", orientation2 = "";
        String[] bits = output.split("\t");
        String chr1 = bits[0], chr2 = "";
        int p1 = Integer.parseInt(bits[1]), p2 = -1;

        String id = bits[2];
        String ref = bits[3];
        String alt = bits[4];
        String qual = bits[5];
        String filter = bits[6];
        String info = bits[7];

        /* get strands */
        pattern = Pattern.compile("STRANDS=(.+?);");
        matcher = pattern.matcher(bits[7]);
        String strands = "";

        if (matcher.find()){
            strands = matcher.group(1);
        }

        if (alt.equals("<INV>")) {
            int posStrand = 0, negStrand = 0;

            /* Get END */
            pattern = Pattern.compile("END=(.+?);");
            matcher = pattern.matcher(bits[7]);

            chr2 = chr1;
            if (matcher.find()){
                p2 = Integer.parseInt(matcher.group(1));
            }

            GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
            GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
            Event e =  new ComplexEvent(c1, c2, EVENT_TYPE.COMPLEX_INVERSION, new Event[] {}, true,null);
            e.setAlt("<CIV>");
            return e;


        } else if (alt.equals("<DEL>") || alt.equals("<DUP>")) {

            String orientation = "";
            /* get orientation */
            pattern = Pattern.compile("STRANDS=(.+?):");
            matcher = pattern.matcher(bits[7]);

            if (matcher.find()){
                orientation = matcher.group(1);
            }

            orientation1 = orientation.substring(0,1);
            orientation2 = orientation.substring(1,1);

            /* Get END */
            pattern = Pattern.compile("END=(.+?);");
            matcher = pattern.matcher(bits[7]);

            chr2 = chr1;
            if (matcher.find()){
                p2 = Integer.parseInt(matcher.group(1));
            }

        } else {
            String[] result = Event.classifyAltGridssLumpy(alt);
            chr2 = result[0];
            p2 = Integer.parseInt(result[1]);
            orientation1 = result[2];
            orientation2 = result[3];
        }

        GenomicCoordinate c1 = new GenomicCoordinate(chr1, p1);
        GenomicCoordinate c2 = new GenomicCoordinate(chr2, p2);
        EVENT_TYPE type = Event.classifySocratesBreakpoint(c1, orientation1, c2, orientation2);

        /*return new Event(c1, c2, type);*/
        return new Event(
                c1, c2, type, id, ref, alt, qual, filter, vcfInfoToHashMap(info),
                new HashSet<Clove.SV_ALGORITHM>() {{add(Clove.SV_ALGORITHM.LUMPY);}}, 1
        );
    }



    public GenomicCoordinate getC1() {
        return c1;
    }

    public GenomicCoordinate  getC2() {
        return c2;
    }

    public EVENT_TYPE getType() {
        return type;
    }

    public void setType( EVENT_TYPE type) {
        this.type = type;
    }

    public void setNode(GenomicNode n, boolean firstCoordinate){
        if(firstCoordinate) {
            if (myNodes.size() > 0) {
                myNodes.set(0, n);
            } else {
                myNodes.add(n);
            }
        } else {
            if (myNodes.size() > 1 ) {
                myNodes.set(1, n);
            } else{
                if( myNodes.size() == 0 ) {
                    myNodes.add(null);
                }
                myNodes.add(n);
            }

        }
    }

    public void setNodes( ArrayList<GenomicNode> nodes){
        nodes.sort(Comparator.comparing(GenomicNode::getStart));

        myNodes = nodes;
    }


    public GenomicNode getNode(boolean firstCoordinate){
        if(this.getNodes().size() > 0 && firstCoordinate) {
            return myNodes.get(0);
        } else if(this.getNodes().size() > 1 && !firstCoordinate){
            return myNodes.get(1);
        }
        return null;
    }

    public ArrayList<GenomicNode> getNodes(){
        return myNodes;
    }


    public static boolean sameNodeSets(Event e1, Event e2){
        if(e1.getNodes().containsAll(e2.getNodes()))
            return true;
        return false;
    }

    @Override
    public String toString() {
        if(c1.onSameChromosome(c2)){
            return this.getId()+" "+c1.getChr()+":"+c1.getPos()+"-"+c2.getPos()+" "+type;
        } else {
            return this.getId()+" "+c1+"<->"+c2+" "+type;
        }
    }

    public ArrayList<GenomicNode> otherNodes(GenomicNode node){

        ArrayList<GenomicNode> others = (ArrayList<GenomicNode>) myNodes.clone();

        if(others.size() > 1) {
            Integer ind = others.indexOf(node);

            if(!ind.equals(null)){
                others.remove(node);
                return others;
            }

            System.err.println("otherNode: query node is not associated with Event! \n" + myNodes.size());
            return null;
        }
        //System.out.println("otherNode: Event has only one node");
        return null;
    }

    public int size() {
        return c1.distanceTo(c2);
    }

//    public void processAdditionalInformation(){
//        if(this.additionalInformation!= null && this.additionalInformation.matches("[ACGT]+") && myNodes.get(0) == myNodes.get(1) ){
//            this.type = EVENT_TYPE.INS;
//        }
//    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getQual() {
        return qual;
    }

    public void setQual(Double qual) {
        this.qual = qual;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getFilter() {
        return (filter==null? ".":filter);
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public boolean getUsed(){ return this.used; }

    public void setUsed(boolean used){this.used = used;}

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    //TODO: Change Info to key-value pairs rather than a string
    public HashMap<String,String> getInfo() {
        return this.info;
    }

    public String getInfo(String key) {
        return this.info.get(key);
    }

    public void setInfo(HashMap<String,String> info) {
        this.info = info;
    }

    public static String getSvTypeVCF(EVENT_TYPE type) {
        if (type.equals(EVENT_TYPE.DEL)) {
            return "DEL";
        } else if (type.equals(EVENT_TYPE.INS)) {
            return "INS";
        } else if (type.equals(EVENT_TYPE.TAN)) {
            return "TAN";
        } else if (type.equals(EVENT_TYPE.INV1) || type.equals(EVENT_TYPE.INV2)) {
            return "INV";
        } else if (type.equals(EVENT_TYPE.ITX1) || type.equals(EVENT_TYPE.ITX2)) {
            return "ITX";
        } else if ( type.equals(EVENT_TYPE.INVTX1) ||type.equals(EVENT_TYPE.INVTX2)){
            return "INVTX";
        } else if(type.equals(EVENT_TYPE.BE1) || type.equals(EVENT_TYPE.BE2)){
            return "BE";
        } else if(type.equals(EVENT_TYPE.COMPLEX_DUPLICATION) || type.equals(EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_DUPLICATION)){
            return "CDU";
        } else if(type.equals(EVENT_TYPE.COMPLEX_INVERTED_TRANSLOCATION)||
                type.equals(EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION)){
            return "CVT";
        } else if(type.equals(EVENT_TYPE.COMPLEX_INVERTED_DUPLICATION) ||
                type.equals(EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_DUPLICATION)){
            return "CVD";
        } else if(type.equals(EVENT_TYPE.COMPLEX_TRANSLOCATION) ||
                type.equals(EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION)){
            return "CTR";
        } else if(type.equals(EVENT_TYPE.COMPLEX_INVERSION)){
            return "CIV";
        } else if(type.equals(EVENT_TYPE.COMPLEX_BIG_INSERTION)){
            return "CIN";
        } else if(type.equals(EVENT_TYPE.COMPLEX_INVERTED_REPLACED_DELETION)){
            return "CVRD";
        } else if( type.equals(EVENT_TYPE.COMPLEX_REPLACED_DELETION)) {
            return "CRDE";
        } else if( type.equals(EVENT_TYPE.COMPLEX_BP)){
            return "CITX";
        } else if( type.equals(EVENT_TYPE.COMPLEX_INVERTED_BP)){
            return "CVTX";
        } else if(type.equals(EVENT_TYPE.VECTOR_PARTS)) {
            return "VEC";
        } else {
            return "XXX";
        }
    }

    public static String getAltVCF(EVENT_TYPE type){
        if(type.equals(EVENT_TYPE.DEL)){
            return "<DEL>";
        } else if(type.equals(EVENT_TYPE.INS)){
            return "<INS>";
        } else if(type.equals(EVENT_TYPE.TAN)){
            return "<TAN>";
        } else if(type.equals(EVENT_TYPE.INV1) || type.equals(EVENT_TYPE.INV2)){
            return "<INV>";
        } else if(type.equals(EVENT_TYPE.ITX1) || type.equals(EVENT_TYPE.ITX2) ||
                type.equals(EVENT_TYPE.INVTX1) || type.equals(EVENT_TYPE.INVTX2) ||
                type.equals(EVENT_TYPE.COMPLEX_BP) || type.equals(EVENT_TYPE.COMPLEX_INVERTED_BP) ){
            return "<BP>";
        } else if(type.equals(EVENT_TYPE.BE1) || type.equals(EVENT_TYPE.BE2)){
            return "<BE>";
        } else if(type.equals(EVENT_TYPE.COMPLEX_DUPLICATION) ||
                type.equals(EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_DUPLICATION)){
            return "<DUP>";
        } else if(type.equals(EVENT_TYPE.COMPLEX_INVERTED_TRANSLOCATION)||
                type.equals(EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION)){
            return "<TRA>";
        } else if(type.equals(EVENT_TYPE.COMPLEX_INVERTED_DUPLICATION) ||
                type.equals(EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_DUPLICATION)){
            return "<DUP>";
        } else if(type.equals(EVENT_TYPE.COMPLEX_TRANSLOCATION) ||
                type.equals(EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION)){
            return "<TRA>";
        } else if(type.equals(EVENT_TYPE.COMPLEX_INVERSION)){
            return "<INV>";
        } else if(type.equals(EVENT_TYPE.COMPLEX_BIG_INSERTION)){
            return "<INS>";
        } else if(type.equals(EVENT_TYPE.COMPLEX_INVERTED_REPLACED_DELETION) ||
                type.equals(EVENT_TYPE.COMPLEX_REPLACED_DELETION)){
            return "<KO>";
        } else if(type.equals(EVENT_TYPE.VECTOR_PARTS)) {
            return "<VEC>";
        } else {
            return "<XXX>";
        }
    }

    public HashSet<Clove.SV_ALGORITHM> getCalledBy() {
        return calledBy;
    }
    public void addCaller(HashSet<Clove.SV_ALGORITHM> caller){
        this.calledBy.addAll(caller);
    }

    public int getCalledTimes() {
        return calledTimes;
    }
    public void increaseCalls(int inc){
        this.calledTimes += inc;
    }

    public String toVcf() {

        // start and end are c1 and 2 always

        String infoString = "";

        infoString = infoString + "SVTYPE" +"=" + this.getInfo().get("SVTYPE") + ";";
        this.getInfo().remove("SVTYPE");

        infoString = infoString + "CHR2" +"=" + this.getInfo().get("CHR2") + ";";
        this.getInfo().remove("CHR2");

        infoString = infoString + "END" +"=" + this.getInfo().get("END") + ";";
        this.getInfo().remove("END");

        for (Map.Entry m:this.getInfo().entrySet()) {
            // System.out.println(m.getKey()+" "+m.getValue());
            infoString=infoString + m.getKey() + "=" +m.getValue() + ";";
        }

        infoString= infoString.substring( 0, infoString.length() - 1);

        return (this.getC1().getChr()+"\t"+this.getC1().getPos()+"\t"+this.getId()+"\t"
                +this.getRef()+"\t"+this.getAlt()+"\t"+ String.format("%.2f",this.getQual()) +"\t"+this.getFilter()
                +"\t"+infoString+";SUPPORT="+this.calledBy.size()+","+this.calledTimes+"\tGT\t1");

    }

    public void addFilter( String filter){
        if(this.getFilter().equals("PASS")){
            this.setFilter(filter);
        } else if (!filter.equals("PASS")){
            this.setFilter(this.getFilter() + ";" + filter);
        }
    }

    public Boolean sameTypes(Event other){
        if(this.getType().equals(other.getType())){
            return true;
        }
//    	else if (( this.getType() == EVENT_TYPE.ITX2 && other.getType() == EVENT_TYPE.ITX1) || (
//				this.getType() == EVENT_TYPE.ITX1 && other.getType() == EVENT_TYPE.ITX2)) {
//    		return true;
//		}
        return false;
    }

    public Boolean checkReadDepth(SamReader samReader, Hashtable<String, Tuple<Double, Double>> readDepthTable, int maxEventSize){

        //TODO: Figure out why this is usually lower than what's in the bedcov file

        double readDepth;
        double mean;
        double interval;

        Tuple<Double, Double> readDepthTuple = readDepthTable.get(this.getC1().getChr());

        if (readDepthTuple != null) {
            mean = readDepthTuple.a;
            interval = readDepthTuple.b;
        } else {
            mean = 0.0;
            interval = 0.0;
        }

//        System.out.println("READ DEPTH: " + this.getC1().toString() + " " + this.getC2().toString());

        switch (this.getType()) {
            case DEL:

                if(this.getC2().getPos() - this.getC1().getPos() < 10){
                    return true;
                }

                readDepth = getReadDepth(samReader, this.getC1().getChr(), this.getC1().getPos() + 1, this.getC2().getPos(), maxEventSize);

                this.getInfo().put("ADP",String.valueOf(readDepth));

                if (mean != 0.0 && (readDepth < 0 || readDepth > mean - interval)) {
                    this.addFilter("RD_FAIL");
                    return false;
                }
                break;

            case TAN:
                if(this.getC2().getPos() - this.getC1().getPos() < 10){
                    return true;
                }

                readDepth = getReadDepth(samReader, this.getC1().getChr(), this.getC1().getPos(), this.getC2().getPos(), maxEventSize);
//                                skipEvents.add(e);
                this.getInfo().put("ADP",String.valueOf(readDepth));

                if (mean != 0.0 && ( readDepth < 0 || readDepth < mean + interval)) {
                    this.addFilter("RD_FAIL");
                    return false;
                }
                break;

            case COMPLEX_INVERSION:
                readDepth = getReadDepth(samReader, this.getC1().getChr(), this.getC1().getPos(), this.getC2().getPos(), maxEventSize);
                this.getInfo().put("ADP",String.valueOf(readDepth));

                if (mean != 0.0 && ( readDepth < mean - interval || readDepth > mean + interval)) {
                    this.addFilter("RD_FAIL");
                    return false;
                }
                break;

            case COMPLEX_DUPLICATION:
            case COMPLEX_INVERTED_DUPLICATION:
            case COMPLEX_INTERCHROMOSOMAL_DUPLICATION:
            case COMPLEX_INTERCHROMOSOMAL_INVERTED_DUPLICATION:
                if (this instanceof ComplexEvent) {
                    ComplexEvent event = (ComplexEvent) this;

                    GenomicCoordinate[] cCoord = event.getInsertionPoint();

                    readDepth = getReadDepth(samReader, cCoord[0].getChr(),
                            cCoord[0].getPos(), cCoord[1].getPos(), maxEventSize);

                    this.getInfo().put("ADP",String.valueOf(readDepth));

                    if (mean != 0.0 && readDepth < mean + interval) {
                        this.addFilter("RD_FAIL");
                        return false;
                    }
                }
                break;

            case COMPLEX_TRANSLOCATION:
            case COMPLEX_INVERTED_TRANSLOCATION:
            case COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION:
            case COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION:
                if (this instanceof ComplexEvent) {
                    ComplexEvent event = (ComplexEvent) this;

                    GenomicCoordinate[] cCoord = event.getInsertionPoint();

                    readDepth = getReadDepth(samReader, cCoord[0].getChr(),
                            cCoord[0].getPos(), cCoord[1].getPos(), maxEventSize);

                    this.getInfo().put("ADP_C",String.valueOf(readDepth));

                    if (mean != 0.0 && (readDepth < mean - interval || readDepth > mean + interval)) {
                        this.addFilter("RD_FAIL");
                        return false;
                    }
                }
                break;

            case COMPLEX_REPLACED_DELETION:
            case COMPLEX_INVERTED_REPLACED_DELETION:
                readDepth = getReadDepth(samReader, this.getC1().getChr(), this.getC1().getPos(), this.getC2().getPos(), maxEventSize);

                double readDepth_dup = getReadDepth(samReader, ((ComplexEvent) this).getInsertionPoint()[0].getChr(),
                        ((ComplexEvent) this).getInsertionPoint()[0].getPos(),
                        ((ComplexEvent) this).getInsertionPoint()[1].getPos(), maxEventSize);


                this.getInfo().put("ADP",String.valueOf(readDepth));
                this.getInfo().put("ADP_C",String.valueOf(readDepth_dup));


                if (mean != 0.0 && (readDepth < 0 || readDepth > mean - interval ||
                        readDepth_dup < mean + interval)) {
                    this.addFilter("RD_FAIL");
                    return false;
                }
                break;

            case COMPLEX_BIG_INSERTION:
                double readDepth_before = getReadDepth(samReader, this.getC1().getChr(), this.getC1().getPos() - 500, this.getC1().getPos(), maxEventSize);
                double readDepth_after = getReadDepth(samReader, this.getC1().getChr(), this.getC2().getPos(), this.getC2().getPos() + 500, maxEventSize);

                this.getInfo().put("ADP",readDepth_before+","+readDepth_after);

                if (mean != 0.0 && (readDepth_before > mean + interval || readDepth_before < mean - interval
                        || readDepth_after > mean + interval || readDepth_after < mean - interval)) {
                    this.addFilter("RD_FAIL");
                    return false;
                }
                break;

            case VECTOR_PARTS:
                readDepth = getReadDepth(samReader, this.getC1().getChr(), this.getC1().getPos(), this.getC2().getPos(), maxEventSize);
                double readDepth_2 = getReadDepth(samReader, this.getC1().getChr(), this.getC1().getPos() - 500, this.getC1().getPos(), maxEventSize);
                double readDepth_3 = getReadDepth(samReader, this.getC2().getChr(), this.getC2().getPos(), this.getC1().getPos() + 500, maxEventSize);


                this.getInfo().put("ADP",readDepth_2 + "," + readDepth + "," + readDepth_3);

                if (mean != 0.0 && (readDepth < mean + interval ||
                        readDepth_2 > mean + interval ||
                        readDepth_3 > mean + interval)) {
                    this.addFilter("RD_FAIL");
                    return false;
                }
                break;

            default:
                break;

        }

        return true;

        // TODO: think about checking for RD jumps ?!
/*                case BE1:
                  case BE2:
                    .addFilter("SINGLE_BE");
                      e.setInfo(e.getInfo()+";INSERTION="+e.getAlt());
                      e.setAlt(getAltVCF(e.getType()));
                      break;
                  case ITX1:
                  case ITX2:
                  case INVTX1:
                  case INVTX2:
                      e.addFilter("SINGLE_ITX");
                      e.setAlt(getAltVCF(e.getType()));
*/

    }

    public double getReadDepth(SamReader samReader, String chr, int start, int end,  int maxEventSize) {

        if (start >= end) {
//			System.err.println("Start bigger than end");
//			return -1;
            int tmp = start;
            start = end;
            end = tmp;
        }

        if (end - start > maxEventSize){
            return -1;
        } else if (end - start < 10){
            // the read depth for very small intervals is not reliable
            return -1;
        } else if (start < 0){
            return -2;
        }


        //SAMFileReader  samReader=new  SAMFileReader(new  File(str));
        ;
        int depth;
        int total = 0;
        int count = 0;

        Interval interval = new Interval(chr, start, end);
        IntervalList iL = new IntervalList(samReader.getFileHeader());
        iL.add(interval);

        SamLocusIterator sli = new SamLocusIterator(samReader, iL, true);

        //SamLocusIterator.setEmitUncoveredLoci(false);

        try {
            for (SamLocusIterator.LocusInfo locusInfo : sli) {
                depth = locusInfo.getRecordAndPositions().size();

                total += depth;
                count++;
            }
            sli.close();
        } catch (Exception e) {
            System.err.println(chr + " " + start + " " + end);
            System.err.println("Error while trying to read BAM file!: " + e.getMessage());
            sli.close();
            return -1;
        }
        //samReader.close();

        return (double) total / count;
    }

    public static HashMap<String,String> vcfInfoToHashMap(String info){
        HashMap<String, String> hashInfo = new HashMap<>();

        for (String part : info.split(";")){
            String[] splitPart = part.split("=");
            if(splitPart.length == 2){
                hashInfo.put(splitPart[0],splitPart[1]);
            }
        }

        return hashInfo;
    }
}
