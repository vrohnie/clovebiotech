package au.edu.wehi.clove;

import htsjdk.samtools.*;
import htsjdk.samtools.util.Tuple;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import static au.edu.wehi.clove.Event.getAltVCF;

class EventIterator implements Iterator<Event> {

    private final Iterator<GenomicNode> myNodeIterator;
    private GenomicNode currentNode;
    private int nextEventIndex;
    private final HashSet<Event> skipEvents;
    private Event currentEvent;

    protected EventIterator(Iterator<GenomicNode> nodeIterator, HashSet<Event> skip) {
        myNodeIterator = nodeIterator;
        currentNode = nodeIterator.next();
        nextEventIndex = 0;
        skipEvents = skip;
    }

    @Override
    public boolean hasNext() {
        System.err.println("Method in EventIterator should not be used!");
        return false;
    }

    @Override
    public Event next() {
        if (currentNode == null) {
            return null;
        }
        if (nextEventIndex < currentNode.getEvents().size()) {
            currentEvent = currentNode.getEvents().get(nextEventIndex);
            nextEventIndex++;
            if (skipEvents.contains(currentEvent))
                return this.next();
            else
                return currentEvent;
        }
        nextEventIndex = 0;
        currentNode = (myNodeIterator.hasNext() ? myNodeIterator.next() : null);
        while (currentNode != null && currentNode.getEvents().size() == 0) {
            if (myNodeIterator.hasNext())
                currentNode = myNodeIterator.next();
            else
                currentNode = null;
        }
        if (currentNode != null)
            return this.next();
        else
            return null;
    }

    public GenomicCoordinate getInsertionCoordinate() {
        if (currentEvent.getNode(true) == currentNode) {
            return currentEvent.getC1();
        } else {
            return currentEvent.getC2();
        }
    }

    @Override
    public void remove() {
        System.err.println("Method in EventIterator should not be used!");
    }

}

public class Clove {
    private static int softclipLength5prime(SAMRecord s) {
        return s.getAlignmentStart() - s.getUnclippedStart();
    }

    private static int softclipLength3prime(SAMRecord s) {
        return s.getUnclippedEnd() - s.getAlignmentEnd();
    }

    private static boolean isInterestingSoftclip(SAMRecord s, int start, int end, String orientation) {
        if (orientation.equals("+") && s.getAlignmentEnd() <= end + 2 && softclipLength5prime(s) > 4) {
            return true;
        }
        if (orientation.equals("-") && s.getAlignmentStart() >= start - 2 && softclipLength3prime(s) > 4) {
            return true;
        }
//		if(s.getAlignmentStart()-2 <= breakpoint && s.getAlignmentEnd()+2>=breakpoint && (softclipLength3prime(s) > 4 || softclipLength5prime(s) > 4))
//			return true;
        return false;
    }

    private static boolean isAlignedAcrossBreakpoint(SAMRecord s, int breakpointPosition) {
        if (s.getAlignmentStart() < breakpointPosition - 5 && s.getAlignmentEnd() > breakpointPosition + 5 && s.getAlignmentStart() - s.getUnclippedStart() < 10 && s.getUnclippedEnd() - s.getAlignmentEnd() < 10)
            return true;
        return false;
    }

    private static double fetchStuff(
            String chr, int start, int end, String orientation, SamReader bamFile) {

        SAMRecordIterator iter = bamFile.queryOverlapping(chr, start, end);

        int properAlignment = 0, softclipped = 0;

        int breakpointPosition = (start + end) / 2;

        for (SAMRecord s; iter.hasNext(); ) {
            s = iter.next();
            if (isInterestingSoftclip(s, start, end, orientation)) {
                softclipped++;
            } else if (isAlignedAcrossBreakpoint(s, breakpointPosition)) {
                properAlignment++;
            }
        }

        return (double) softclipped / (softclipped + properAlignment);
    }

    private static void mergeEventNodes(Hashtable<String, TreeSet<GenomicNode>> genomicNodes, Integer maxDistanceForNodeMerge, Boolean checkRedundantEvents, Boolean checkEventNodesDistinct) {

        for (Entry<String, TreeSet<GenomicNode>> tableEntry : genomicNodes.entrySet()) {
            int nodesMerged = 0;
            GenomicNode[] staticList = new GenomicNode[tableEntry.getValue().size()];

            // TreeSet should be sorted automatically
            tableEntry.getValue().toArray(staticList);

            if (staticList.length == 0)
                break;
            GenomicNode lastNode = staticList[0], currentNode = null;
            for (int i = 1; i < staticList.length; i++) {
                currentNode = staticList[i];

                boolean validNodeMerge = true;

                if (currentNode.getStart().distanceTo(lastNode.getEnd()) < maxDistanceForNodeMerge) {
                    if (checkEventNodesDistinct){
                        for (Event event : currentNode.getEvents()){
                            if((event.otherNodes(currentNode) != null) && event.otherNodes(currentNode).contains(lastNode)){
                                validNodeMerge = false;
                                break;
                            }
                        }

                    }
                } else {
                    validNodeMerge = false;
                }

                if(validNodeMerge) {
                        lastNode.mergeWithNode(currentNode);
                        if (!tableEntry.getValue().remove(currentNode))
                            System.out.println("Couldn't remove node");
                        nodesMerged++;
                } else {
                    if (checkRedundantEvents)
                        lastNode.checkForRedundantEvents(maxDistanceForNodeMerge);
                    lastNode = currentNode;
                }
            }
            if (checkRedundantEvents) {
                lastNode.checkForRedundantEvents(maxDistanceForNodeMerge);
                if(currentNode != null) {
                    currentNode.checkForRedundantEvents(maxDistanceForNodeMerge);
                }
            }
            System.out.println("Nodes merged: " + nodesMerged);
        }
    }

//    private static void mergeEventNodesByCov(Hashtable<String, TreeSet<GenomicNode>> genomicNodes, Integer maxDistanceForNodeMerge, SamReader samReader){
//        for (Entry<String, TreeSet<GenomicNode>> tableEntry : genomicNodes.entrySet()) {
//            int nodesMerged = 0;
//            GenomicNode[] staticList = new GenomicNode[tableEntry.getValue().size()];
//            tableEntry.getValue().toArray(staticList);
//            if (staticList.length == 0)
//                break;
//            GenomicNode lastNode = staticList[0], currentNode = null;
//            for (int i = 1; i < staticList.length; i++) {
//                currentNode = staticList[i];
//
//                double readDepth = getReadDepth(samReader, currentNode.getStart().getChr(), lastNode.getStart().getPos(), currentNode.getStart().getPos());
//
//                if (currentNode.getStart().distanceTo(lastNode.getEnd()) < maxDistanceForNodeMerge) {
//
//                    lastNode.mergeWithNode(currentNode);
//                    if (!tableEntry.getValue().remove(currentNode))
//                        System.out.println("Couldn't remove node");
//                    nodesMerged++;
//                } else {
//                    lastNode = currentNode;
//                }
//            }
//            System.out.println("Nodes merged: " + nodesMerged);
//        }
//    }

    private static void addEventToNodeList(Event e, Hashtable<String, TreeSet<GenomicNode>> genomicNodes, boolean useFirstCoordinate) {
        GenomicCoordinate coordinate = (useFirstCoordinate ? e.getC1() : e.getC2());
        //if (coordinate.getPos() != -1 && !coordinate.getChr().isEmpty()) {
        TreeSet<GenomicNode> nodeSet;
        if (!genomicNodes.containsKey(coordinate.getChr())) {
            nodeSet = new TreeSet<GenomicNode>();
            genomicNodes.put(coordinate.getChr(), nodeSet);
        } else {
            nodeSet = genomicNodes.get(coordinate.getChr());
        }
        GenomicNode newNode = new GenomicNode(coordinate, e);
        e.setNode(newNode, useFirstCoordinate);
        nodeSet.add(newNode);
        //}
    }

    private static String generateNodeLabel(GenomicNode n) {
        return n.getStart().getChr() + "_" + n.getStart().getPos() + "_" + n.getEnd().getPos();
    }

    private static void graphVisualisation(String outputFilename, Hashtable<String, TreeSet<GenomicNode>> genomicNodes) throws IOException {
        HashSet<Event> eventsWritten = new HashSet<Event>();
        FileWriter output = new FileWriter(outputFilename);
        output.write("digraph g {\n");
        for (Entry<String, TreeSet<GenomicNode>> tableEntry : genomicNodes.entrySet()) {
            if (!tableEntry.getKey().equals("ecoli"))
                continue;
            output.write("{rank=same; ");
            for (GenomicNode n : tableEntry.getValue()) {
                output.write(generateNodeLabel(n) + "; ");
            }
            output.write("}\n");
            for (GenomicNode n : tableEntry.getValue()) {
                for (Event e : n.getEvents()) {
                    if (eventsWritten.contains(e))
                        continue;
                    else
                        eventsWritten.add(e);
                    String l1 = generateNodeLabel(e.getNode(true)), l2 = generateNodeLabel(e.getNode(false));
                    switch (e.getType()) {
                        case DEL:
                            output.write(l1 + "->" + l2 + "[label=\"DEL\"];\n");
                            break;
                        case TAN:
                            output.write(l1 + "->" + l2 + "[label=\"TAN\" arrowtail=normal arrowhead=none dir=both];\n");
                            break;
                        case COMPLEX_INVERSION:
                            Event ee = ((ComplexEvent) e).getEventsInvolvedInComplexEvent()[0];
                            l1 = generateNodeLabel(ee.getNode(true));
                            l2 = generateNodeLabel(ee.getNode(false));
                            //if (l1.equals("ecoli_28204143_28204160")){
                            //if (l1.equals("ecoli_24118329_24118583")){
//							System.out.println("COMPLEX_INV:");
//							System.out.println("l1:\t"+generateNodeLabel(ee.getNode(true))+"\t");
//							System.out.println("l2:\t"+generateNodeLabel(ee.getNode(false))+"\t");
                            //}
                            output.write(l1 + "->" + l2 + "[label=\"COMPLEX_INV\"  dir=both];\n");
                            break;//should we differentiate COMPLEX_INV and INV?
                        case COMPLEX_TRANSLOCATION:
                        case COMPLEX_DUPLICATION:
                            GenomicNode insNode = e.getNode(true);
                            String label = (e.getType() == EVENT_TYPE.COMPLEX_DUPLICATION ? "DUP" : "TRANS");
                            l1 = generateNodeLabel(insNode);
                            Event[] allEvents = ((ComplexEvent) e).getEventsInvolvedInComplexEvent();
                            for (Event allEvent : allEvents) {
                                if (allEvent.getNode(true) == insNode) {
                                    l2 = generateNodeLabel(allEvent.getNode(false));
                                    output.write(l1 + "->" + l2 + "[label=\"" + label + "\" arrowtail=odiamond arrowhead=normal dir=both];\n");
                                } else if (allEvent.getNode(false) == insNode) {
                                    l2 = generateNodeLabel(allEvent.getNode(true));
                                    output.write(l1 + "->" + l2 + "[label=\"" + label + "\" arrowtail=odiamond arrowhead=normal dir=both];\n");
                                }

                            }
                            break;
                        default:
                            output.write(l1 + "->" + l2 + "[label=\"" + e.getType() + "\"];\n");
                    }
                }
            }
        }
        output.write("}\n");
        output.flush();
        output.close();
    }


    private static void compareToGoldStandard(String goldFileName, Hashtable<String, TreeSet<GenomicNode>> genomicNodes, int margin, boolean compareStrictly) throws IOException {
        boolean checkAgain = true;
        if (oldFns.size() == 0) {
            checkAgain = false;
        }

        BufferedReader gold = new BufferedReader(new FileReader(goldFileName));
        String goldLine = gold.readLine();
        String currentChromosome = goldLine.replace(":", "\t").split("\t")[1];
        System.out.println("Working on first chromosome: " + currentChromosome);
        //Iterator<GenomicNode> iter = genomicNodes.get("gi|260447279|gb|CP001637.1|").iterator();

        HashSet<Event> skip = new HashSet<Event>();
        HashSet<Event> tryAgain = new HashSet<Event>();
        HashSet<String> recalledOnce = new HashSet<String>();
        Iterator<GenomicNode> iter = genomicNodes.get(currentChromosome).iterator();
        EventIterator events = new EventIterator(iter, skip);
        Event e = events.next();

        Hashtable<EVENT_TYPE, int[]> statsByType = new Hashtable<EVENT_TYPE, int[]>();
        for (EVENT_TYPE t : EVENT_TYPE.values()) {
            //the convention used below is: TP index 0, FP 1, and FN 2
            statsByType.put(t, new int[4]);
        }
        //static conversion table
        Hashtable<String, EVENT_TYPE> typeConversion = new Hashtable<String, EVENT_TYPE>();
        {
            typeConversion.put("INVERSION", EVENT_TYPE.COMPLEX_INVERSION);
            typeConversion.put("DELETION", EVENT_TYPE.DEL);
            typeConversion.put("TANDEM", EVENT_TYPE.TAN);
            //typeConversion.put("INSERTION", EVENT_TYPE.INS);
            //TODO: Chack why Insertion and Duplication are mixed
            typeConversion.put("INSERTION", EVENT_TYPE.COMPLEX_DUPLICATION);
            typeConversion.put("TRANSLOCATION", EVENT_TYPE.COMPLEX_TRANSLOCATION);
            typeConversion.put("INVERTED_TRANSLOCATION", EVENT_TYPE.COMPLEX_INVERTED_TRANSLOCATION);
            typeConversion.put("INVERTED_INSERTION", EVENT_TYPE.COMPLEX_INVERTED_DUPLICATION);
            typeConversion.put("DUPLICATION", EVENT_TYPE.COMPLEX_DUPLICATION);
            typeConversion.put("INVERTED_DUPLICATION", EVENT_TYPE.COMPLEX_INVERTED_DUPLICATION);
            typeConversion.put("INTERCHROMOSOMAL_TRANSLOCATION", EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION);
            typeConversion.put("INTERCHROMOSOMAL_INVERTED_TRANSLOCATION", EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION);
            typeConversion.put("INTERCHROMOSOMAL_DUPLICATION", EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_DUPLICATION);
            typeConversion.put("INTERCHROMOSOMAL_INVERTED_DUPLICATION", EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_DUPLICATION);
        }


        while (goldLine != null) {
            StringTokenizer st = new StringTokenizer(goldLine, ":-\t ");
            String type = st.nextToken();
            String chr = st.nextToken();
            //TODO: what if the SVs are on a different chromosome?
            if (!currentChromosome.equals(chr)) {
                while (e != null) {
                    if (tryAgain.contains(e)) {
                        System.out.println("FPFPFPFP: " + e);
                        statsByType.get(e.getType())[1]++;
                    } else {
                        tryAgain.add(e);
                    }
                    e = events.next();
                }

                currentChromosome = chr;
                System.out.println("Working on chromosome: " + currentChromosome);
                if (!genomicNodes.containsKey(chr)) {
                    e = null;
                    continue;
                }
                iter = genomicNodes.get(currentChromosome).iterator();
                events = new EventIterator(iter, skip);
                e = events.next();
            }

            int start = Integer.parseInt(st.nextToken());
            //int end = Integer.parseInt(st.nextToken());
            if (type.equals("SNP") || type.equals("JOIN") || type.equals("TRANSLOCATION_DELETION")) {
                goldLine = gold.readLine();
                continue;
            }
            if (e == null) {
                //System.out.println("DEFINITE FN: "+goldLine);
                statsByType.get(typeConversion.get(type))[2]++;
                goldLine = gold.readLine();
                continue;
            }

            GenomicCoordinate compare;
            if (e.getType() == EVENT_TYPE.COMPLEX_INVERTED_TRANSLOCATION || e.getType() == EVENT_TYPE.COMPLEX_INVERTED_DUPLICATION
                    || e.getType() == EVENT_TYPE.COMPLEX_DUPLICATION || e.getType() == EVENT_TYPE.COMPLEX_TRANSLOCATION
                    || e.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION || e.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_DUPLICATION
                    || e.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_DUPLICATION || e.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION) {
                compare = ((ComplexEvent) e).getInsertionPoint()[1];
                tryAgain.add(e); // do not attempt single coordinate events twice.
            } else if (tryAgain.contains(e)) {
                compare = events.getInsertionCoordinate();
            } else {
                compare = events.getInsertionCoordinate();
            }
            if (!compare.getChr().equals(chr)) {
                //Fusion on different chr than goldLine
                if (compare.getChr().compareTo(chr) < 0) {
                    System.out.println("DEFAULT FP? " + e);
                    e = events.next();
                    continue;
                } else {
                    System.out.println("DEFAULT FN?");
                    goldLine = gold.readLine();
                    continue;
                }
            }

            //GenomicCoordinate compare = (e.getType() == EVENT_TYPE.COMPLEX_INVERTED_TRANSLOCATION || e.getType() == EVENT_TYPE.COMPLEX_INVERTED_DUPLICATION || e.getType() == EVENT_TYPE.COMPLEX_DUPLICATION || e.getType() == EVENT_TYPE.COMPLEX_TRANSLOCATION? ((ComplexEvent)e).getInsertionPoint() : e.getC1());
            if (compare.distanceTo(new GenomicCoordinate(chr, start)) > margin) {
                if (compare.compareTo(new GenomicCoordinate(chr, start)) < 0) {
                    //half TP?
                    if (tryAgain.contains(e) || compareStrictly) {
                        //System.out.println("FP: "+e);
                        statsByType.get(e.getType())[1]++;
                    } else {
                        tryAgain.add(e);
                    }
                    e = events.next();
                } else {
                    if (!recalledOnce.contains(goldLine) || compareStrictly) {
                        System.out.println("FN: " + goldLine);
                        if (checkAgain && !oldFns.contains(goldLine)) {
                            System.out.println("New FN: " + goldLine);
                        } else if (!checkAgain) {
                            oldFns.add(goldLine);
                        }
                        statsByType.get(typeConversion.get(type))[2]++;
                    }
                    goldLine = gold.readLine();
                    continue;
                }
            } else {
                if (typeConversion.get(type) == e.getType()) {
//				if(type.equals("INVERSION") && e.getType()==EVENT_TYPE.COMPLEX_INVERSION || type.equals("DELETION") && e.getType()==EVENT_TYPE.DEL
//						|| type.equals("TANDEM") && e.getType()==EVENT_TYPE.TAN || type.equals("INSERTION") && e.getType()==EVENT_TYPE.COMPLEX_DUPLICATION
//						|| type.equals("TRANSLOCATION") && e.getType()==EVENT_TYPE.COMPLEX_TRANSLOCATION || type.equals("INSERTION") && e.getType()==EVENT_TYPE.INS) {
                    //System.out.println("TP: "+e+" "+goldLine);
                    if (recalledOnce.contains(goldLine)) {
                        //redundant TP?
                        System.out.println("Redundant TP!");
                    } else {
                        statsByType.get(e.getType())[0]++;
                    }
                    //goldLine = gold.readLine();


                } else {
                    //System.out.println("Half TP: Type mismatch: "+e+" "+goldLine);
                    if (recalledOnce.contains(goldLine)) {
                        //redundant HTP?
                        System.out.println("Redundant HTP!");
                    } else {
                        statsByType.get(typeConversion.get(type))[3]++;
                    }

                }
                recalledOnce.add(goldLine);
                skip.add(e);
                e = events.next();
            }

        }
        while (goldLine != null) {
            if (!goldLine.contains("SNP") && !goldLine.contains("TRANSLOCATION_DELETION") && (!recalledOnce.contains(goldLine) || compareStrictly)) {
                String type = (new StringTokenizer(goldLine)).nextToken();
                System.out.println("FN: " + goldLine);
                statsByType.get(typeConversion.get(type))[2]++;
            }
            goldLine = gold.readLine();
        }
        e = events.next();

        int tps = 0, fps = 0, fns = 0, htps = 0;
        System.out.println("Stats:\tTP\tHalf-TP\tFP\tFN\tSen\tSpe");
        for (EVENT_TYPE t : EVENT_TYPE.values()) {
            int[] stats = statsByType.get(t);
            double sen = (stats[0] + stats[2] + stats[3] == 0 ? 0 : (double) (stats[0] + stats[3]) / (stats[0] + stats[2] + stats[3]));
            double spe = (stats[0] + stats[1] + stats[3] == 0 ? 0 : (double) (stats[0] + stats[3]) / (stats[0] + stats[1] + stats[3]));
            System.out.println(t + "\t" + stats[0] + "\t" + stats[3] + "\t" + stats[1] + "\t" + stats[2] + "\t" + sen + "\t" + spe);
            tps += stats[0];
            fps += stats[1];
            fns += stats[2];
            htps += stats[3];
        }
        System.out.println("Total\t" + tps + "\t" + htps + "\t" + fps + "\t" + fns + "\t" + ((double) (tps + htps) / (tps + htps + fns)) + "\t" + ((double) (tps + htps) / (tps + htps + fps)));
        System.out.println("Accuracy:\t" + ((double) tps / (tps + fps + fns)) + "\t" + ((double) (tps + htps) / (tps + fps + fns + htps)));
        gold.close();
    }

    private static void reportEventComposition(Hashtable<String, TreeSet<GenomicNode>> genomicNodes) {
        Hashtable<EVENT_TYPE, Integer> eventCounts = new Hashtable<EVENT_TYPE, Integer>();
        int selfRef = 0;
        for (EVENT_TYPE t : EVENT_TYPE.values()) {
            eventCounts.put(t, 0);
        }
        HashSet<Event> skipEvents = new HashSet<Event>();
        for (Entry<String, TreeSet<GenomicNode>> tableEntry : genomicNodes.entrySet()) {
            for (GenomicNode n : tableEntry.getValue()) {
                for (Event e : n.getEvents()) {
                    if (skipEvents.contains(e))
                        continue;
                    Integer i = eventCounts.get(e.getType()) + 1;
                    eventCounts.put(e.getType(), i);
                    if ((e.otherNodes(n) != null) && e.otherNodes(n).get(0) == n) {
                        selfRef++;
                    } else
                        skipEvents.add(e);
                }
            }
        }
        for (EVENT_TYPE t : EVENT_TYPE.values()) {
            System.out.println(t + ": " + eventCounts.get(t));
        }
        System.out.println("Self refs: " + selfRef);
    }

    public static void createVCFHeader(PrintWriter output, ArrayList<String> header, String command, String sample) throws IOException {
        //file format

        ArrayList<String> info = new ArrayList<>();
        ArrayList<String> info_ids = new ArrayList<>();
        ArrayList<String> filter = new ArrayList<>();
        ArrayList<String> filter_ids = new ArrayList<>();
        ArrayList<String> format = new ArrayList<>();
        ArrayList<String> format_ids = new ArrayList<>();
        ArrayList<String> contig = new ArrayList<>();
        ArrayList<String> contig_ids = new ArrayList<>();

        //TODO: See why this is not working the way it was intended
        for(String headerline : header){
            String id;

            if(headerline.startsWith("##INFO=")){
                id = headerline.substring(headerline.indexOf("ID=")+3,
                        headerline.indexOf(","));

                if(!info_ids.contains(id)){
                    info.add(headerline);
                    info_ids.add(id);
                }
            } else if(headerline.startsWith("##FILTER=")){
                id = headerline.substring(headerline.indexOf("ID=")+3,
                        headerline.indexOf(","));

                if(!filter_ids.contains(id)){
                    filter.add(headerline);
                    filter_ids.add(id);
                }
            } else if(headerline.startsWith("##FORMAT=")){
                id = headerline.substring(headerline.indexOf("ID=")+3,
                        headerline.indexOf(","));

                if(!format_ids.contains(id)){
                    format.add(headerline);
                    format_ids.add(id);
                }
            } else if(headerline.startsWith("##contig=")){
                id = headerline.substring(headerline.indexOf("ID=")+3,
                        headerline.indexOf(","));

                if(!contig_ids.contains(id)){
                    contig.add(headerline);
                    contig_ids.add(id);
                }
            }
        }

        output.write("##fileformat=VCFv4.2\n");

        //fileDate
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        output.write("##fileDate=" + dateFormat.format(date) + "\n");

        output.write("##INFO=<ID=SVTYPE,Number=1,Type=String,Description=\"Exact type of structural Variant\">\n");
        output.write("##INFO=<ID=CHR2,Number=1,Type=String,Description=\"Chromosome of SV End coordinate\">\n");
        output.write("##INFO=<ID=END,Number=1,Type=Integer,Description=\"Position of SV End coordinate\">\n");
        output.write("##INFO=<ID=INSERTION,Number=1,Type=String,Description=\"Inserted bases at Breakpoint or Breakend\">\n");
        output.write("##INFO=<ID=CHR_C,Number=1,Type=String,Description=\"Chromosome of additional breakpoints in a complex event\">\n");
        output.write("##INFO=<ID=START_C,Number=1,Type=Integer,Description=\"Start position of additional breakpoints in a complex event\">\n");
        output.write("##INFO=<ID=END_C,Number=1,Type=Integer,Description=\"End position of additional breakpoints in a complex event\">\n");
        output.write("##INFO=<ID=ADP,Number=.,Type=Float,Description=\"Average Read Depth, if two values means before and after BP/INS\">\n");
        output.write("##INFO=<ID=ADP_C,Number=.,Type=Float,Description=\"Average Read Depth, around the other event site in complex events\">\n");
        output.write("##INFO=<ID=SUPPORT,Number=2,Type=Integer,Description=\"SV support by (i) number of aligners, and (ii) number of calls between all aligners\">\n");


        //FILTER
        for(String line : filter){
            output.write(line + "\n");
        }

        output.write("##FILTER=<ID=RD_FAIL,Description=\"Read depth check for the complex event failed.\">\n");
        output.write("##FILTER=<ID=SINGLE_ITX,Description=\"Single ITX event with no matching other interchromosomal Changes.\">\n");
        output.write("##FILTER=<ID=SINGLE_INV,Description=\"Single Inversion event with no matching Inversion event.\">\n");
        output.write("##FILTER=<ID=SINGLE_BE,Description=\"Single Breakend with no other matching events.\">\n");

        //FORMAT
        output.write("##FORMAT=<ID=GT,Number=.,Type=String,Description=\"No format information is added\">\n");

        //ALT
        output.write("##ALT=<ID=DEL,Description=\"Deletion\">\n");
        output.write("##ALT=<ID=TAN,Description=\"Tandem Duplication (TAN= single BP, CTA= complex TAN RD checked, CVTA= complex inverted Tandem)\">\n");
        output.write("##ALT=<ID=INV,Description=\"Inversion (INV= single BP, CIV= compex Inversion, 2BPs)\">\n");
        output.write("##ALT=<ID=INS,Description=\"Insertion (INS=small Insertion, CIN= Complex/BIG Insertion)\">\n");
        output.write("##ALT=<ID=DUP,Description=\"Complex Duplication (CDU= complex Duplication, CVT=Complex Inverted Duplication)\">\n");
        output.write("##ALT=<ID=TRA,Description=\"Translocation (CTR=Complex Translocation, CVT=Complex Inverted Translocation)\">\n");
        output.write("##ALT=<ID=BP,Description=\"Single breakpoint (BP=within chromsome, ITX=interchromosomal BP)\">\n");
        output.write("##ALT=<ID=BE,Description=\"Breakend\">\n");
        output.write("##ALT=<ID=KO,Description=\"Knockout (CRDE=Complex Replaced Deletion, CVRD=Inverted Replaced Deletion, CRDI= Deletion, replaced by Insertion)\">\n");
        output.write("##ALT=<ID=VEC,Description=\"Breakends flanking high coverage region, could be part of an integration Vector or other kind of integrated or not integrated DNA sequence\">\n");

        for(String line : contig){
            output.write(line + "\n");
        }

        output.write("##CloveCommand="+ command +"\n");

        //Header Line
        output.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t"+sample+"\n");
    }

    enum SV_ALGORITHM {SOCRATES, DELLY, DELLY2, CREST, GUSTAF, BEDPE, METASV, GRIDSS, LUMPY};


    static ArrayList<String> oldFns = new ArrayList<String>();

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        //Start Time
        long startTime = System.nanoTime();

        if (args.length < 8) {
            System.err.println("Options ( mandatory -- input can be specified more than once):" +
                    "\n\t-i <list of breakpoints> <algorithm (Socrates/Delly/Delly2/Crest/Gustaf/BEDPE/GRIDSS)>" +
                    "\n\t-b <BAM file> +" +
                    "\n( optional ):\" +" +
                    "\n\t-c <Coverage BED file> [default: calculate mean/std of coverage per contig]" +
                    "\n\t-o <output filename> [default: CLOVE.vcf]" +
                    "\n\t-s <sample name> [default: sample]" +
                    "\n\t-r <Max size for which to check read depth> [default: 10000]" +
                    "\n\t-r Do not perform read depth check. Runs a lot faster but ignores read depth pattern." +
                    "\n\t Use to get an idea about complex variants only.");
            System.exit(0);
        }

        /*parse the options from the command line */
        int argindex = 0;
        ArrayList<Tuple<BufferedReader, SV_ALGORITHM>> inputs = new ArrayList<Tuple<BufferedReader, SV_ALGORITHM>>();
        Hashtable<String, Tuple<Double, Double>> readDepthTable = new Hashtable<>();

        SamReader samReader = null;
        double mean = 0;
        double interval = 0;
        String goldStandard = null;
        String outputVCF = "CLOVE.vcf";
        boolean checkRD = true;
        boolean detectVectorParts = false;
        int maxEventSize = 10000;
        String sample= "sample";

        while (argindex < args.length) {
            if (args[argindex].equals("-i")) {
                try {
                    BufferedReader input = new BufferedReader(new FileReader(args[argindex + 1]));
                    SV_ALGORITHM algorithm = SV_ALGORITHM.valueOf(args[argindex + 2].toUpperCase());
                    inputs.add(new Tuple<BufferedReader, Clove.SV_ALGORITHM>(input, algorithm));
                    argindex += 3;
                } catch (IllegalArgumentException e) {
                    System.err.println("Unable to parse input breakpoints.");
                    System.exit(1);
                }
            } else if (args[argindex].equals("-b")) {
                try {
                    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
                    samReader = samReaderFactory.open(new File(args[argindex + 1]));
                    argindex += 2;
                } catch (Exception e) {
                    System.err.println("Unable to load bam file.");
                    System.exit(1);
                }
            } else if (args[argindex].equals("-c")) {

                try {
                    BufferedReader rd_input = new BufferedReader(new FileReader(args[argindex + 1]));
                    String line;

                    while ((line = rd_input.readLine()) != null) {
                        String[] bits = line.split("\t");

                        if (bits.length != 3) {
                            throw new IllegalArgumentException("Malformed Read Depth input!");
                        } else {
                            mean = Double.parseDouble(bits[1]);
                            interval = Double.parseDouble(bits[2]);
                            readDepthTable.put(bits[0], new Tuple<>(mean, interval));
                        }

                    }

                    argindex += 2;
                }
//				try{
//					mean = Double.parseDouble(args[argindex + 1]);
//					interval = Double.parseDouble(args[argindex + 2]);
//					argindex += 3;
//				}
                catch (IllegalArgumentException e) {
                    System.err.println(e.getMessage());
                    System.exit(1);
                }
            } else if (args[argindex].equals("-d")) {
                goldStandard = args[argindex + 1];
                argindex += 2;
            } else if (args[argindex].equals("-o")) {
                outputVCF = args[argindex + 1];
                argindex += 2;
            } else if (args[argindex].equals("-s")) {
                sample = args[argindex + 1];
                argindex += 2;
            } else if (args[argindex].equals("-v")) {
                detectVectorParts = true;
            } else if (args[argindex].equals("-r")){
				if (argindex + 1 < args.length && !args[argindex + 1].startsWith("-")) {
                    maxEventSize = Integer.valueOf(args[argindex + 1]);
                    argindex += 2;
                } else {
                    checkRD = false;
                    argindex++;
                }
			}
            else {
                System.err.println("Unknown option: " + args[argindex]);
                throw new IllegalArgumentException();
            }
        }


        /*
         * parse the entire input file and collect all events in list
         */
        ArrayList<Event> allEvents = new ArrayList<Event>();
        ArrayList<String> header = new ArrayList<>();

        String line;
        int count = 0;

        for (Tuple<BufferedReader, SV_ALGORITHM> input_tuple : inputs) {
            System.out.println("Reading input...");
            BufferedReader input = input_tuple.a;
            SV_ALGORITHM algorithm = input_tuple.b;
            while ((line = input.readLine()) != null) {
                //TODO: make # check algorithm specific?
                if (line.startsWith("#")) {
                    header.add(line);
                    continue;
                }

                try {
                    Event e;

                    switch (algorithm) {
                        case SOCRATES:
                            e = Event.createNewEventFromSocratesOutputLatest(line, count++);
                            break;
                        case DELLY:
                            e = Event.createNewEventFromDellyOutputLatest(line);
                            break;
                        case DELLY2:
                            e = Event.createNewEventFromDelly2Output(line);
                            break;
                        case CREST:
                            e = Event.createNewEventFromCrestOutputLatest(line, count++);
                            break;
                        case GUSTAF:
                            e = Event.createNewEventFromGustafOutput(line);
                            if (e.size() < 50) continue;
                            break;
                        case BEDPE:
                            e = Event.createNewEventFromBEDPE(line);
                            break;
                        case METASV:
                            e = Event.createNewEventFromMetaSVOutput(line);
                            break;
                        case GRIDSS:
                            e = Event.createNewEventFromGRIDSSOutput(line);
                            //if (e.getType() == EVENT_TYPE.BE1 || e.getType() == EVENT_TYPE.BE2) continue;
                            break;
                        case LUMPY:
                            e = Event.createNewEventFromLUMPYOutput(line);
                            break;
                        default:
                            e = null;
                            break;
                    }

                    // System.out.println("P1 " + e.toString());
                    allEvents.add(e);

                } catch (Exception ex) {
                    System.err.println("Corrupted VCF File, Error Message is: " + ex.getMessage());
                    System.exit(1);
                }

            }
            input.close();
        }
        System.out.println("Total events: " + allEvents.size());

        PrintWriter writer = new PrintWriter(outputVCF, "UTF-8");
        createVCFHeader(writer, header, Arrays.toString(args), sample);

        /*
         * Create nodes data structure that combines close events into the same
         * genomic node
         */
        Hashtable<String, TreeSet<GenomicNode>> genomicNodes = new Hashtable<String, TreeSet<GenomicNode>>();

        //parse all events and create new nodes
        for (Event e : allEvents) {
            addEventToNodeList(e, genomicNodes, true);
            addEventToNodeList(e, genomicNodes, false);
        }

        //static parameter to classify single inversions as FP or TP
        final boolean classifySimpleInversion = false;

        //iterate through node sets and merge nodes where necessary
        //also checks each node for redundant members

        mergeEventNodes(genomicNodes, 15, true, false);

        System.out.println("Events merged: " + GenomicNode.global_event_merge_counter);

        //String goldStandard = args[1].substring(0, 22)+"_2.fa";
        //String goldStandard = "/home/users/allstaff/schroeder/GenotypeBreakpoints/data/ecoli/SV_list_2.txt";


        //compareToGoldStandard(goldStandard, genomicNodes, 150, true);
        if (goldStandard != null)
            compareToGoldStandard(goldStandard, genomicNodes, 150, false);

        String tempInfo = null;
        // iterate through node sets again, and genotype events
        // genomicNodes equal chromosomes or contigs

        for (Entry<String, TreeSet<GenomicNode>> tableEntry : genomicNodes.entrySet()) {
            HashSet<GenomicNode> removeNodes = new HashSet<GenomicNode>();
            System.out.println("Nodes on chr " + tableEntry.getKey() + ": " + tableEntry.getValue().size());

            // table Entry contains all nodes on one chromosome/contig
            // nodes are conglomerations of events (SVs/BPs/BEs)

            for (GenomicNode currentNode : tableEntry.getValue()) {


                if (currentNode.getStart().getPos() == -1) {
                    //handling the null node introduced by breakends
                    continue;
                }

                Event e1, e2;
                HashSet<Event> removeEvents = new HashSet<Event>();
                HashSet<ComplexEvent> newComplexEvents = new HashSet<ComplexEvent>();
                ComplexEvent newComplexEvent = null;

                //iterate through all event-event pairing in this node and assess for complex events

                ArrayList<Event> currentEvents = currentNode.getEvents();
                currentEvents.sort(Comparator.comparingDouble(Event::getQual).reversed());

                // System.out.println("New Node: " + currentNode.toString());

                for (int i = 0; i < currentEvents.size(); i++) {
                    e1 = currentEvents.get(i);

//                    System.out.println("Main Event: " + e1.toString() + " " + e1.getQual().toString());

                    for (int j = 0; j < currentEvents.size(); j++) {

                        EVENT_TYPE eventType = null;

                        e2 = currentEvents.get(j);

//                        System.out.println("Event_2: " + e2.toString() + " " + e2.getQual().toString());

                        GenomicCoordinate eventStart = null, eventEnd = null;
                        GenomicCoordinate[] eventInsert = new GenomicCoordinate[]{null, null};
                        GenomicNode hostingNode = currentNode;
                        Event[] involvedEvents = new Event[]{e1, e2};

                        // skip removed events // Why would I want that? and events where both nodes are the same
                        if (e1 == e2 || removeEvents.contains(e2) || removeEvents.contains(e1))
                                // || e1.otherNodes(currentNode).get(0) == currentNode || e2.otherNodes(currentNode).get(0) == currentNode)
                            continue;
                        switch (e1.getType()) {
                            //inversions
                            case INV1: {
                                // System.out.println(e1.toString() + e2.toString());

                                if (e2.getType() == EVENT_TYPE.INV2 && Event.sameNodeSets(e1, e2)) {
                                    //System.out.println("Complex inversion between "+e1+" and "+e2);
                                    eventStart = (e1.getC1().compareTo(e1.getC2()) < 0 ? e1.getC1() : e1.getC2());
                                    eventEnd = (e2.getC2().compareTo(e2.getC1()) < 0 ? e2.getC1() : e2.getC2());

                                    eventType = EVENT_TYPE.COMPLEX_INVERSION;

                                } else if (e2.getType() == EVENT_TYPE.INV2) {
                                    GenomicNode other1 = e1.otherNodes(currentNode).get(0), other2 = e2.otherNodes(currentNode).get(0);

                                    if (other1.compareTo(other2) > 0) {
                                        Event e3 = other1.existsDeletionEventTo(other2);
                                        if (e3 != null) {

                                            eventType = EVENT_TYPE.COMPLEX_INVERTED_TRANSLOCATION;
                                            involvedEvents = new Event[]{e1, e2, e3};

                                        } else {
                                            eventType = EVENT_TYPE.COMPLEX_INVERTED_DUPLICATION;
                                        }

                                        eventStart = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                        eventEnd = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventInsert[0] = (e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1());
                                        eventInsert[1] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                    }
                                } else {
//                                    System.out.println("PAIRING FAILED");
                                    //unknown pairing
                                }
                                break;
                            }
                            //duplications and translocations
                            case DEL: {
                                if (e2.getType() == EVENT_TYPE.TAN) {
                                    GenomicNode other1 = e1.otherNodes(currentNode).get(0), other2 = e2.otherNodes(currentNode).get(0);
                                    if (other1.compareTo(other2) < 0 && currentNode.compareTo(other1) < 0
                                            || other2.compareTo(other1) < 0 && other1.compareTo(currentNode) < 0) {
                                        Event e3 = other1.existsDeletionEventTo(other2);

                                        if (e3 != null) {
                                            // System.out.println("Translocation between "+e1+ " and "+ e2);
                                            // intrachromosomal translocations are actually ambiguous as to where they come from and got to
                                            // as a convention, we call the smaller bit the translocated one inserted into the larger bit.
                                            if (e1.size() < e3.size()) {
                                                //area under e1 is translocated

                                                //assuming ordered coordinates
                                                eventInsert[0] = e1.getC1();
                                                eventInsert[1] = e1.getC2();

                                                eventStart = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                                eventEnd = (e3.getNode(true) == other2 ? e3.getC1() : e2.getC2());

                                            } else {
                                                //area under e3 is translocated
                                                eventStart = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                                eventEnd = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());

                                                eventInsert[0] = e3.getC1();
                                                eventInsert[1] = e3.getC2();

                                            }
                                            eventType = EVENT_TYPE.COMPLEX_TRANSLOCATION;
                                            involvedEvents = new Event[]{e1, e2, e3};
                                        } else {
                                            //System.out.println("Duplication between "+e1+ " and "+ e2);

                                            if (other1.compareTo(other2) < 0) {

                                                //duplicated bit is downstream of currentNode
                                                eventInsert[0] = (e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1());
                                                eventInsert[1] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());

                                                eventStart = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                                eventEnd = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());

                                                eventType = EVENT_TYPE.COMPLEX_DUPLICATION;

                                            } else {
                                                eventInsert[0] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                                eventInsert[1] = (e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1());

                                                eventStart = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                                eventEnd = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());

                                                eventType = EVENT_TYPE.COMPLEX_DUPLICATION;
                                            }

                                        }
                                    }
                                }
                                break;
                            }
                            //interchromosomal events
                            case ITX1: {
                                if (e2.getType() == EVENT_TYPE.ITX2) {
                                    GenomicNode other1 = e1.otherNodes(currentNode).get(0), other2 = e2.otherNodes(currentNode).get(0);
                                    if (!other1.getStart().onSameChromosome(other2.getStart()))
                                        break;
                                    if (other1.equals(other2)) {
                                        //TODO: think about handling balanced Translocations
                                        eventStart = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventEnd = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                    } else if (currentNode.compareTo(other1) < 0 && other1.getEnd().compareTo(other2.getStart()) < 0) {
                                        eventStart = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventEnd = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                        eventInsert[0] = (e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1());
                                        eventInsert[1] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());

                                    } else if (currentNode.compareTo(other1) > 0 && other1.getStart().compareTo(other2.getEnd()) > 0) {
                                        eventStart = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                        eventEnd = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventInsert[0] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                        eventInsert[1] = (e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1());
                                    } else {
                                        break;
                                    }

//                                    if (eventStart.compareTo(eventEnd) >= 0) {
//                                        System.out.println("Start > End! Shouldn't happen");
//                                    }

                                    Event e3 = other1.existsDeletionEventTo(other2);
                                    if (eventInsert[0] ==  null){

                                        eventType = EVENT_TYPE.COMPLEX_BP;

                                     } else if (e3 != null) {

                                        eventType = EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION;
                                        involvedEvents = new Event[]{e1, e2, e3};

                                    } else if ((other1.getStart().getPos() < 15) || (other2.getStart().getPos() < 15)) {
                                        eventType = EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION;

                                    } else {
                                        eventType = EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_DUPLICATION;
                                    }
                                }
                                break;
                            }
                            case INVTX1: {

                                if (e2.getType() == EVENT_TYPE.INVTX2) {

                                    GenomicNode other1 = e1.otherNodes(currentNode).get(0), other2 = e2.otherNodes(currentNode).get(0);
                                    if (!other1.getStart().onSameChromosome(other2.getStart())){
                                        break;
                                    }

                                    if (other1.equals(other2)){
                                        eventStart = (e1.getNode(true).compareTo(e1.getNode(false)) < 0 ? e1.getC1() : e1.getC2());
                                        eventEnd = (e2.getNode(true).compareTo(e2.getNode(false)) > 0 ? e2.getC1() : e2.getC2());
                                    } else if (currentNode.compareTo(other1) < 0 && other1.getEnd().compareTo(other2.getStart()) > 0) {
                                        eventStart = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventEnd = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                        eventInsert[0] = (e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1());
                                        eventInsert[1] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                    } else if (currentNode.compareTo(other1) > 0 && other1.getStart().compareTo(other2.getEnd()) > 0) {
                                        eventStart = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                        eventEnd = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventInsert[0] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                        eventInsert[1] = (e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1());
                                    } else {
                                        break;
                                    }

                                    Event e3 = other1.existsDeletionEventTo(other2);

                                    // TODO: think of better way to check for complete insertion of plasmid
                                    if (eventInsert[0] ==  null){

                                        eventType = EVENT_TYPE.COMPLEX_INVERTED_BP;

                                    } else if (e3 != null ) {
                                        // System.out.println("EV3: " + e3.toString());
                                        eventType = EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION;
                                        involvedEvents = new Event[]{e1, e2, e3};
                                    } else if ((other1.getStart().getPos() < 15) || (other2.getStart().getPos() < 15)) {
                                        eventType = EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_TRANSLOCATION;
                                    } else {
                                        eventType = EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_INVERTED_DUPLICATION;

                                    }

                                }
                                break;
                            }
                            case BE1: {

                                // Ensuring that BE's and BP's are facing towards each other
                                List<EVENT_TYPE> rightBeSmall = Arrays.asList(EVENT_TYPE.ITX2,EVENT_TYPE.TAN);
                                List<EVENT_TYPE> rightBeBig = Arrays.asList(EVENT_TYPE.ITX1, EVENT_TYPE.DEL);

                                if (e2.getType() == EVENT_TYPE.BE2 || e2.getType() == EVENT_TYPE.INV2 || e2.getType() == EVENT_TYPE.INVTX2 ||
                                        (e2.getNode(true).equals(currentNode) &&
                                                rightBeSmall.contains(e2.getType())) ||
                                        (e2.getNode(false).equals(currentNode) &&
                                                rightBeBig.contains(e2.getType()))) {
                                    if (e2.getType() == EVENT_TYPE.BE2) {
                                        eventStart = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventEnd = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                    } else {
                                        eventStart = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventEnd = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                        eventInsert[0] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                    }

                                    eventType = EVENT_TYPE.COMPLEX_BIG_INSERTION;

                                }
                            }

                            case BE2: {
                                // Ensuring that BE's and BP's are facing towards each other
                                List<EVENT_TYPE> leftBeBig = Arrays.asList(EVENT_TYPE.ITX2, EVENT_TYPE.TAN);
                                List<EVENT_TYPE> leftBeSmall = Arrays.asList(EVENT_TYPE.ITX1, EVENT_TYPE.DEL);

                                if (e2.getType() == EVENT_TYPE.BE1 || e2.getType() == EVENT_TYPE.INVTX1 || e2.getType() == EVENT_TYPE.INV1 ||
                                                (e2.getNode(true).equals(currentNode) &&
                                                        leftBeSmall.contains(e2.getType())) ||
                                                (e2.getNode(false).equals(currentNode) &&
                                                        leftBeBig.contains(e2.getType()))) {

                                    if (e2.getType() == EVENT_TYPE.BE1) {
                                        eventStart = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                        eventEnd = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                    } else {
                                        eventStart = (e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2());
                                        eventEnd = (e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2());
                                        eventInsert[0] = (e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1());
                                    }

                                    eventType = EVENT_TYPE.COMPLEX_BIG_INSERTION;

                                }
                            }

                            default: //don't even attempt other types
                        }
                        //check if a new complex event has been generated

                        if (eventStart != null) {

                            if (eventInsert[0] != null) {
                                newComplexEvent = new ComplexEvent(eventStart, eventEnd, eventType, involvedEvents, true, hostingNode, eventInsert);

                            } else {
                                newComplexEvent = new ComplexEvent(eventStart, eventEnd, eventType, involvedEvents, true, hostingNode);
                            }

                        }

                        if (newComplexEvent != null) {
                            //-> add events to cleanup and break current loop

                            newComplexEvents.add(newComplexEvent);
                            //System.out.println("New Event is: "+ newComplexEvent.toString());

                            // Only remove simple events if read depth check passes
                            boolean removeSingles = checkRD?newComplexEvent.checkReadDepth(samReader, readDepthTable, maxEventSize):true;


                            for (Event e : newComplexEvent.getEventsInvolvedInComplexEvent()) {

                                e.setUsed(true);

                                // As this event is not an enclosed cycle we might need the ITX events once again

                                if (newComplexEvent.getType() == EVENT_TYPE.COMPLEX_BIG_INSERTION &&
                                        (e.getType() == EVENT_TYPE.ITX1 || e.getType() == EVENT_TYPE.ITX2 ||
                                                e.getType() == EVENT_TYPE.INVTX1 || e.getType() == EVENT_TYPE.INVTX2)) {
                                    continue;
                                }

                                if( removeSingles ) {
                                    removeEvents.add(e);
                                }

                                //	System.out.println("Is removed "  + e.toString());

                            }

                            newComplexEvent = null;

                            if (removeSingles) {
                                break; //break the for-j loop, because BP is paired correctly
                            }
                        }
                    }
                }
                //all event pairings have been investigated
                //-> clean up some stuff by removing events and adding the new complex ones.

                for (Event e : newComplexEvents) {
                    e.getNode(true).getEvents().add(e);
                }

                for (Event e : removeEvents) {

                    e.getNode(true).getEvents().remove(e);
                    e.getNode(false).getEvents().remove(e);

                    if (e.getNode(true).getEvents().size() == 0) {
                        //System.out.println("Node is empty");
                        removeNodes.add(e.getNode(true));
                    }
                    if (e.getNode(false).getEvents().size() == 0) {
                        //System.out.println("Node is empty");
                        removeNodes.add(e.getNode(false));
                    }
                }

            }

            for (GenomicNode n : removeNodes) {
                tableEntry.getValue().remove(n);
            }
        }

        mergeEventNodes(genomicNodes, 100000, false, true);

        System.out.println("Events merged in second run: " + GenomicNode.global_event_merge_counter);

        // This time we iterate to find more specific SVs like Knockouts and "unconnected Duplications",
        // results are searched for on

        for (Entry<String, TreeSet<GenomicNode>> tableEntry : genomicNodes.entrySet()) {
            HashSet<GenomicNode> removeNodes = new HashSet<GenomicNode>();
            //System.out.println("Nodes on chr " + tableEntry.getKey() + ": " + tableEntry.getValue().size());
            for (GenomicNode currentNode : tableEntry.getValue()) {

                //iterate through all event-event pairing in this node and assess for complex events

                if (currentNode.getStart().getPos() == -1) {
                    //this is created by all the breakends that have no real second coordinate
                    continue;
                }

                Event e1, e2;
                HashSet<Event> removeEvents = new HashSet<Event>();
                HashSet<Event> vecSkipEvents = new HashSet<Event>();

                HashSet<ComplexEvent> newComplexEvents = new HashSet<ComplexEvent>();
                ComplexEvent newComplexEvent = null;

                ArrayList<Event> currentEvents = currentNode.getEvents();
                currentEvents.sort(Comparator.comparingDouble(Event::getQual).reversed());

                for (int i = 0; i < currentEvents.size(); i++) {
                    e1 = currentEvents.get(i);

                    //System.out.println("P3: " + e1.toString());

                    for (Event currentEvent : currentEvents) {
                        boolean skip = false;

                        if (!detectVectorParts) {
                            skip = true;
                        }

                        e2 = currentEvent;

                        // skip removed events and events where both nodes are the same
                        if (e1 == e2 || removeEvents.contains(e2) || removeEvents.contains(e1)
                                || (e1.otherNodes(currentNode) != null && e1.otherNodes(currentNode).get(0) == currentNode)
                                || (e2.otherNodes(currentNode) != null && e2.otherNodes(currentNode).get(0) == currentNode) )
                            continue;

                        if (vecSkipEvents.contains(e2) || vecSkipEvents.contains(e1))
                            skip = true;

                        GenomicCoordinate eventStart = null, eventEnd = null;
                        GenomicCoordinate[] eventReplacement = new GenomicCoordinate[]{null, null};
                        EVENT_TYPE type = null;

                        switch (e1.getType()) {
                            //inversions
                            case DEL:
                            case ITX1:
                                if (e2.getType() == EVENT_TYPE.ITX2 || e2.getType() == EVENT_TYPE.TAN) {

//                                    System.out.println(e1.toString() + " : " + e2.toString());

                                    if (Event.sameNodeSets(e1, e2)) {
                                        if ((e2.getNode(true) == e1.getNode(true)) &&
                                                (e1.getC1().compareTo(e2.getC1()) == (e1.getC2().compareTo(e2.getC2())))) {
                                            //that' right, else it would be weird

                                            eventStart = (e1.getC1().compareTo(e2.getC1()) < 0 ? e1.getC1() : e2.getC2());
                                            eventEnd = (e1.getC1().compareTo(e2.getC1()) < 0 ? e2.getC1() : e1.getC2());
                                            eventReplacement[0] = (e1.getC1().compareTo(e2.getC1()) < 0 ? e1.getC2() : e2.getC1());
                                            eventReplacement[1] = (e1.getC1().compareTo(e2.getC1()) < 0 ? e2.getC2() : e1.getC1());

                                            type = EVENT_TYPE.COMPLEX_REPLACED_DELETION;

                                        }
                                    } else if (!skip) {
                                        if (e1.getNode(true) == currentNode && e2.getNode(true) == currentNode &&
                                                e1.getC1().compareTo(e2.getC1()) > 0) {

                                            eventStart = e2.getC1();
                                            eventEnd = e1.getC1();
                                            eventReplacement[0] = e1.getC2();
                                            type = EVENT_TYPE.VECTOR_PARTS;

                                        } else if (e1.getNode(false) == currentNode && e2.getNode(false) == currentNode &&
                                                e1.getC2().compareTo(e2.getC2()) < 0) {

                                            eventStart = e1.getC2();
                                            eventEnd = e2.getC2();
                                            eventReplacement[0] = e1.getC1();
                                            type = EVENT_TYPE.VECTOR_PARTS;

                                        }
                                    }
                                } else if (!skip) {
                                    if ((e2.getType() == EVENT_TYPE.INVTX1 || e2.getType() == EVENT_TYPE.INV1)
                                            && e1.getNode(false) == currentNode) {

                                        eventStart = e1.getC2();
                                        eventEnd = e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2();
                                        eventReplacement[0] = e1.getC1();
                                        type = EVENT_TYPE.VECTOR_PARTS;

                                    } else if ((e2.getType() == EVENT_TYPE.INVTX2 || e2.getType() == EVENT_TYPE.INV2)
                                            && e1.getNode(true) == currentNode) {

                                        eventStart = e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2();
                                        eventEnd = e1.getC1();
                                        eventReplacement[0] = e1.getC2();
                                        type = EVENT_TYPE.VECTOR_PARTS;

                                    } else if (e2.getType() == EVENT_TYPE.ITX1 || e2.getType() == EVENT_TYPE.DEL) {
                                        if (e1.getNode(true) == currentNode && e2.getNode(false) == currentNode &&
                                                e1.getC1().compareTo(e2.getC2()) > 0) {

                                            eventStart = e2.getC2();
                                            eventEnd = e1.getC1();
                                            eventReplacement[0] = e1.getC2();
                                            type = EVENT_TYPE.VECTOR_PARTS;
                                        }

                                    }
                                }

                                break;

                            case TAN:
                            case ITX2:
                                if (!skip && (e2.getType() == EVENT_TYPE.ITX2 || e2.getType() == EVENT_TYPE.TAN) &&
                                        e1.getNode(true) == currentNode && e2.getNode(false) == currentNode &&
                                        e1.getC1().compareTo(e2.getC2()) < 0) {
                                    eventStart = e1.getC1();
                                    eventEnd = e2.getC2();
                                    eventReplacement[0] = e1.getC2();
                                    type = EVENT_TYPE.VECTOR_PARTS;

                                }

                                break;

                            case INV1:
                            case INVTX1:
                                if (e2.getType() == EVENT_TYPE.INVTX2 || e2.getType() == EVENT_TYPE.INV2) {
                                    if (Event.sameNodeSets(e1, e2) &&
                                            (e1.getC1().compareTo(e2.getC2()) != (e1.getC2().compareTo(e2.getC1())))) {
                                        // Complex InterChromosomal INVERTED Replacement

                                        if (e2.getNode(true) == e1.getNode(true) &&
                                                e1.getC1().compareTo(e2.getC1()) == e2.getC2().compareTo(e1.getC2())) {
                                            // that' right, else it would be weird

                                            eventStart = (e1.getC1().compareTo(e2.getC1()) < 0 ? e1.getC1() : e1.getC2());
                                            eventEnd = (e1.getC1().compareTo(e2.getC1()) < 0 ? e2.getC1() : e2.getC2());

                                            eventReplacement[0] = (e1.getC1().compareTo(e2.getC1()) < 0 ? e2.getC2() : e2.getC1());
                                            eventReplacement[1] = (e1.getC1().compareTo(e2.getC1()) < 0 ? e1.getC2() : e1.getC1());

                                            type = EVENT_TYPE.COMPLEX_INVERTED_REPLACED_DELETION;

                                        }
                                    } else if (!skip) {

                                        eventStart = e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2();
                                        eventEnd = e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2();
                                        eventReplacement[0] = e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1();

                                        if (eventStart.compareTo(eventEnd) > 0) {
                                            eventStart = null;
                                        }

                                        type = EVENT_TYPE.VECTOR_PARTS;
                                    }

                                } else if (!skip && (e2.getType() == EVENT_TYPE.ITX2 || e2.getType() == EVENT_TYPE.TAN)) {

                                    eventStart = e2.getNode(true) == currentNode ? e2.getC1() : null;
                                    eventEnd = e1.getNode(true) == currentNode ? e1.getC1() : e1.getC2();
                                    eventReplacement[0] = e1.getNode(true) == currentNode ? e1.getC2() : e1.getC1();
                                    type = EVENT_TYPE.VECTOR_PARTS;


                                }

                                break;
                            case BE1:
                                if (!skip) {
                                    if (e2.getNode(true) == currentNode && e2.getC1().compareTo(e1.getC1()) < 0 &&
                                            (e2.getType() == EVENT_TYPE.BE2 || e2.getType() == EVENT_TYPE.ITX2 || e2.getType() == EVENT_TYPE.TAN)) {

                                        //that' right, else it would be weird

                                        eventStart = e2.getC1();
                                        eventEnd = e1.getC1();

                                        if (e2.getType() == EVENT_TYPE.ITX2) {
                                            eventReplacement[0] = e2.getC2();
                                        }

                                        type = EVENT_TYPE.VECTOR_PARTS;

                                    } else if (e2.getType() == EVENT_TYPE.INVTX2 || e2.getType() == EVENT_TYPE.INV2) {
                                        eventStart = e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2();
                                        eventEnd = e1.getC1();
                                        eventReplacement[0] = e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1();

                                        if (eventStart.compareTo(eventEnd) > 0) {
                                            eventStart = null;
                                        }

                                        type = EVENT_TYPE.VECTOR_PARTS;

                                    } else if (e2.getNode(false) == currentNode && e2.getC2().compareTo(e1.getC1()) < 0 &&
                                            (e2.getType() == EVENT_TYPE.ITX1 || e2.getType() == EVENT_TYPE.DEL)) {
                                        //that' right, else it would be weird

                                        eventStart = e2.getC2();
                                        eventEnd = e1.getC1();
                                        eventReplacement[0] = e2.getC1();
                                        type = EVENT_TYPE.VECTOR_PARTS;

                                    }
                                }
                                break;

                            case BE2:
                                if (!skip) {
                                    if (e2.getNode(true) == currentNode && e2.getC1().compareTo(e1.getC1()) > 0 &&
                                            (e2.getType() == EVENT_TYPE.BE1 || e2.getType() == EVENT_TYPE.ITX1 || e2.getType() == EVENT_TYPE.DEL)) {

                                        //that' right, else it would be weird

                                        eventStart = e1.getC1();
                                        eventEnd = e2.getC1();

                                        if (e2.getType() == EVENT_TYPE.ITX1 || e2.getType() == EVENT_TYPE.DEL) {
                                            eventReplacement[0] = e2.getC2();
                                        }

                                        type = EVENT_TYPE.VECTOR_PARTS;


                                    } else if (e2.getType() == EVENT_TYPE.INVTX1 || e2.getType() == EVENT_TYPE.INV1) {
                                        eventStart = e1.getC1();
                                        eventEnd = e2.getNode(true) == currentNode ? e2.getC1() : e2.getC2();
                                        eventReplacement[0] = e2.getNode(true) == currentNode ? e2.getC2() : e2.getC1();

                                        if (eventStart.compareTo(eventEnd) > 0) {
                                            eventStart = null;
                                        }

                                        type = EVENT_TYPE.VECTOR_PARTS;

                                    } else if (e2.getNode(false) == currentNode && e2.getC2().compareTo(e1.getC1()) > 0 &&
                                            (e2.getType() == EVENT_TYPE.ITX2 || e2.getType() == EVENT_TYPE.TAN)) {
                                        //that' right, else it would be weird

                                        eventStart = e1.getC1();
                                        eventEnd = e2.getC2();
                                        eventReplacement[0] = e2.getC1();
                                        type = EVENT_TYPE.VECTOR_PARTS;

                                    }
                                }
                                break;

                            default: //don't even attempt other types
                                break;

                        }

                        if (eventStart != null) {

                            if (eventReplacement[0] != null) {
                                newComplexEvent = new ComplexEvent(eventStart, eventEnd, type, (new Event[]{e1, e2}), true, currentNode, eventReplacement);
                            } else {
                                newComplexEvent = new ComplexEvent(eventStart, eventEnd, type, (new Event[]{e1, e2}), true, currentNode);
                            }

                        }

                        //check if a new complex event has been generated
                        if (newComplexEvent != null) {
                            //-> add events to cleanup and break current loop

                            boolean removeSingles = checkRD ? newComplexEvent.checkReadDepth(samReader, readDepthTable, maxEventSize) : true;

                            //System.out.println(newComplexEvent.toString());
                            newComplexEvents.add(newComplexEvent);
                            for (Event e : newComplexEvent.getEventsInvolvedInComplexEvent()) {
                                e.setUsed(true);
                                // As this event is not an enclosed cycle we might need the ITX events once again
                                if (newComplexEvent.getType() == EVENT_TYPE.VECTOR_PARTS) {
                                    vecSkipEvents.add(e);
                                } else if (removeSingles) {
                                    removeEvents.add(e);
                                }
                            }
                            newComplexEvent = null;

                            if (removeSingles) {
                                break; //break the for-j loop, as this guy is already paired
                            }
                        }
                    }
                }

                for (Event e : newComplexEvents) {
//                  System.out.println(e.toString());
//                  System.out.println(e.getNode(true));
                    e.getNode(true).getEvents().add(e);
                }

                for (Event e : removeEvents) {
                    e.getNode(true).getEvents().remove(e);
                    e.getNode(false).getEvents().remove(e);

                    if (e.getNode(true).getEvents().size() == 0) {
                        removeNodes.add(e.getNode(true));
                    }
                    if (e.getNode(false).getEvents().size() == 0) {
                        removeNodes.add(e.getNode(false));
                    }
                }


            }

            for (GenomicNode n : removeNodes) {
                tableEntry.getValue().remove(n);
            }

        }


        //while we're at it: let's run through the nodes again!
        //this time for output
        //TODO: remove/skip already used single breakpoints

        int totalEvents = 0;
        for (Entry<String, TreeSet<GenomicNode>> tableEntry : genomicNodes.entrySet()) {
            //System.out.println("Working on Entry: "+tableEntry.toString());
            for (GenomicNode currentNode : tableEntry.getValue()) {

                if (currentNode.getStart().getPos() == -1) {
                    //this is created by all the breakends that have no real second coordinate
                    continue;
                }

                totalEvents += currentNode.getEvents().size();
                HashSet<Event> skipEvents = new HashSet<Event>(), deleteEvents = new HashSet<Event>(), newEvents = new HashSet<Event>();

                for (Event e : currentNode.getEvents()) {

                    //System.out.println("P4: " + e.toString());

                    if (skipEvents.contains(e))
                        continue;
                    // if(currentNode.getEvents().size() < 2 && e instanceof ComplexEvent ){//&& e.otherNode(currentNode) != currentNode){// (e.getType() == EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_DUPLICATION || e.getType()==EVENT_TYPE.COMPLEX_INTERCHROMOSOMAL_TRANSLOCATION)){
                    // e.processAdditionalInformation(); //TODO: this is a bit of a sly hack to classify insertions in Socrates... not sure how to do it more transparently.

                    boolean rd_pass;

                    switch (e.getType()) {
                        // handles all Inversions
                        case INV1:
                        case INV2:
                            if(e.getUsed()){
//                                System.out.println(e);
                                deleteEvents.add(e);
                            }
                  /*          else if (classifySimpleInversion) {
                                skipEvents.add(e);
                                ComplexEvent e2 = new ComplexEvent(e.getC1(), e.getC2(), EVENT_TYPE.COMPLEX_INVERSION, new Event[]{e}, true, currentNode);
                                e = e2;
                            } */
                            else {
                                e.setAlt("<BP>");
                                e.addFilter("SINGLE_ITX");
                            }
                            break;
                        case DEL:
                            //check for deletion
                            int delLen = e.getC2().getPos()-e.getC1().getPos();
                            String insert = e.getInfo("INSERTION");

//                            System.out.println(e.getInfo("INSERTION"));

                            if( insert != null && insert.length() > delLen){
                                e.setAlt(getAltVCF(EVENT_TYPE.INS));
                                //e.setType(EVENT_TYPE.INS);
                                e.getInfo().put("SVTYPE","INS");
                            } else {
                                e.setAlt(getAltVCF(EVENT_TYPE.DEL));
                            }

                            rd_pass = checkRD?e.checkReadDepth(samReader, readDepthTable, maxEventSize):true;

                            if(! rd_pass && e.getUsed()){
                                deleteEvents.add(e);
                            }
                            break;
                        case TAN:
                            e.setAlt("<TAN>");

                            rd_pass = checkRD?e.checkReadDepth(samReader, readDepthTable, maxEventSize):true;

                            if(! rd_pass && e.getUsed()){
                                deleteEvents.add(e);
                            }
                            break;
                        case COMPLEX_BIG_INSERTION:
                            break;
                        // handles all interchromosmal events
                        case BE1:
                        case BE2:
                            e.addFilter("SINGLE_BE");
                            e.getInfo().put("INSERTION",e.getAlt());
                            e.setAlt(getAltVCF(e.getType()));
                            if(e.getUsed()){
                                deleteEvents.add(e);
                            }
                            break;
                        case ITX1:
                        case ITX2:
                        case INVTX1:
                        case INVTX2:
                            e.addFilter("SINGLE_ITX");
                            e.setAlt(getAltVCF(e.getType()));
                            if(e.getUsed()){
                                deleteEvents.add(e);
                            }
                            break;
                        default:
                            break;
                    }

                    //System.out.println(e);
                    //}
                    if ( e.otherNodes(currentNode) != null && e.otherNodes(currentNode).get(0) == currentNode) {
                        skipEvents.add(e);
                        //System.out.println("Self reference: "+e);
                    } else {
                        //System.out.println(e);
                        if (e.otherNodes(currentNode) != null) {
                            e.otherNodes(currentNode).get(0).getEvents().remove(e);
                        }
                    }
                }

                currentNode.getEvents().addAll(newEvents);
                for (Event e : deleteEvents) {
                    e.getNode(true).getEvents().remove(e);
                    e.getNode(false).getEvents().remove(e);
                }
            }
        }
        System.out.println("Total events: " + totalEvents);

        //compareToGoldStandard(goldStandard, genomicNodes, 150, true);
        if (goldStandard != null)
            compareToGoldStandard(goldStandard, genomicNodes, 150, false);

        //graphVisualisation("data/simul_ecoli_graph.gv", genomicNodes);

        count = 0;
        //reportEventComposition(genomicNodes);
        /*VCF Output*/
        HashSet<Event> skipEvents = new HashSet<Event>();
        for (Entry<String, TreeSet<GenomicNode>> tableEntry : genomicNodes.entrySet()) {
            for (GenomicNode currentNode : tableEntry.getValue()) {

                if (currentNode.getStart().getPos() == -1) {
                    //this is created by all the breakends that have no real second coordinate
                    continue;
                }

                for (Event e : currentNode.getEvents()) {
                    if (skipEvents.contains(e)) {
                        continue;
                    } else {
                        skipEvents.add(e);
                    }
                    try {
                        //System.out.println("Trying to write vcf: " + e.toVcf());
                        writer.write(e.toVcf() + "\n");
                    } catch (NullPointerException npe) {
                        System.out.println("VCF fail");
                        System.err.println(npe);
                    }
                }
            }
        }

        writer.close();
        if(samReader != null){
            samReader.close();
        }
        //End Time
        long endTime = System.nanoTime();
        System.out.println("Took " + (endTime - startTime) / 1000000000 + " seconds");
    }


}
