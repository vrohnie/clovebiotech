# CloveBiotech: Complex SV calling for Biotech applications

CloveBiotech is an extension of Clove (https://github.com/PapenfussLab/clove). It scans the 
output of one or more sets of simple SV or fusion calls. This version has been extended
to output breakends, as caused by large insertions or targeted palsmid integration, 
typical KO patterns and substitutions.

It checks read depth patterns, allowing different inputs for each contig

## INSTALLING CloveBiotech

    >Download the jar file from the bitbucket clovebiotech/Downloads

        java -jar clovebiotech.v*.*.jar ...

    >Use docker

        run -it -v $PWD:/data -w data vrohnie/clovebiotech:v1.0.1

## RUNNING CloveBiotech

CloveBiotech runs on java 1.8 or higher. 
Invoke CloveBiotech with java -jar <release.jar>
This prompts you with the following parameters:

    Mandatory Input:	
        -i <list of breakpoints> <algorithm (SOCRATES/DELLY2/CREST/GUSTAF/BEDPE/GRIDSS/METASV/LUMPY)> (an be specified more than once)
        -b <BAM file> 

    Optional:
        -o <output filename> [default: CLOVE.vcf]
        -r <Max length for which to check read depth patterns> [default: 10000] or
        -r Do not perform read depth check. Runs a lot faster but ignores read depth pattern.
        -c <Coverage BED file> [default: calculate mean/std of coverage per contig]
	
An example run of CloveBiotech could look like this: 

	`java -jar clovebiotech_v1.0.1.jar -i svs.vcf GRIDSS -i delly.vcf DELLY2  -b my_bam.bam -c coverage.bed -r 10000 -o my_calls.vcf`
	
This will take the input in my_results.txt and my_bam.bam to produce calls in my_calls.vcf.


## A FEW NOTES

1. The input bam file has to be sorted and indexed, as CloveBiotech is random accessing it.
2. Input files are not filtered. Filter annotations of all tools are kept and additional ones are added.
3. The support field in the VCF ("SUP") reflects how many calls are contributing to the event. The algorithm will also report "Events merged: X" on the command line to indicate if this has happened (X>0).
4. The "coverage variance" parameter is used as an interval around the mean: All read depths outside this interval qualify for deletions (low values) or duplications.

5. The coverage bed file has to be provided as follows (Look up the examples in the TestData Folder):
    chr1\t100\t10
    chr2\t100\t10
    mit\t900\t90

## Licence

Published under GNU General Public License v2.0